<?php



$firstminut = new CPT( array(

    'post_type_name' => 'firstminut',

    'singular'       => __('First minut', 'commitment'),

    'plural'         => __('First minut', 'commitment'),

    'slug'           => 'firstminut'

),

	array(

    'show_in_rest' => true,

    'supports'  => array( 'custom-fields'),

    'menu_icon' => 'dashicons-admin-home',
    
    'publicly_queryable'  => false

));
