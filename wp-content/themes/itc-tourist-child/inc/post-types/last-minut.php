<?php



$lastminut = new CPT( array(

    'post_type_name' => 'lastminut',

    'singular'       => __('Last minut', 'commitment'),

    'plural'         => __('Last minut', 'commitment'),

    'slug'           => 'lastminut'

),

	array(

    'show_in_rest' => true,

    'supports'  => array( 'custom-fields' ),

    'menu_icon' => 'dashicons-admin-home',

    'publicly_queryable'  => false

));

