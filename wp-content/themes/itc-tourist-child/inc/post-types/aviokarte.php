<?php



$aviokarte = new CPT( array(

    'post_type_name' => 'aviokarte',

    'singular'       => __('Avio karte', 'commitment'),

    'plural'         => __('Avio karte', 'commitment'),

    'slug'           => 'aviokarte'

),

	array(

    'show_in_rest' => true,

    'supports'  => array( 'custom-fields' ),

    'menu_icon' => 'dashicons-admin-home'

));



$aviokarte -> register_taxonomy( array(

    'taxonomy_name' => 'aviokarte_cat',

    'singular'      => __('Avio karte', 'commitment'),

    'plural'        => __('Avio karte', 'commitment'),

    'slug'          => 'aviokarte-category'

));