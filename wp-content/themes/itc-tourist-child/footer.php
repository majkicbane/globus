<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

?>

	</main><!-- #content -->

	<footer id="colophon" class="site-footer">
		<!-- Contact sidebar (START) -->
		<div id="contactSidebarButton">
			<i class="icon-telephone-bold"></i>
		</div>
		<div class="focus-dark-bg"></div>
		<div class="contact-card">
			<h2>Kontakt</h2>
			<div class="info d-flex flex-column">
				<div class="d-flex">
					<div class="d-flex flex-column align-items-center">
						<i class="icon-location"></i>
						<i class="icon-telephone"></i>
						<i class="icon-smartphone"></i> 
						<i class="icon-mail"></i>
					</div>
					<div class="d-flex flex-column">
						<span><?php the_field('adresa_firme','option'); ?></span>
						<span><?php the_field('contact_phone','option'); ?></span>
						<span><?php the_field('mobilni_telefon','option'); ?></span>
						<span><?php the_field('contact_email','option'); ?></span>
					</div>
				</div>
				
				<div class="d-flex">
					<i class="icon-clock"></i>
					<div class="d-flex justify-content-between">
						<div class="d-flex flex-column">
							<span>Pon-Pet:</span>
							<span>Subota:</span>
							<span>Nedelja:</span>
						</div>
						<div class="d-flex flex-column">
							<span><?php the_field('mon_fri','option'); ?></span>
							<span><?php the_field('sat','option'); ?></span>
							<span><?php the_field('sun','option'); ?></span>
						</div>
					</div>
				</div>

				<div class="d-flex flex-column mt-2">
					<?php $form =  get_field('mapa_kk','option') ?>
					<?php echo do_shortcode($form); ?>
					<a href="<?php echo get_site_url(); ?>/globus/avio-karte/" class="second-btn mt-4 w-100"> Kontaktiraj nas </a>
				</div>
			</div>
		</div>
		<!-- Contact sidebar (END) -->

		<!-- Mobile filters (START) -->
		<?php if(wp_is_mobile()) : ?>
			<?php if(!(get_page_template_slug()=='templates/template-rezultati.php')) : ?>
				<div id="mobFilterButton">
					<i class="icon-search"></i>
				</div>
				<div class="focus-dark-bg-filters"></div>
				<div class="mob-filter-card">
					<div class="filter-box">
						
							<?php 
								echo '<div id="apartmaniFilter">';
								require('templates/function/filter-apartmani.php'); 
								echo '</div>';
							?>
						
					</div>
				</div>
			<?php endif; ?>
		<?php endif; ?>
		<!-- Mobile filters (END) -->

		<div class="container footer-content">
			<div class="row">
				<div class="col-xl-3 col-lg-4 col-md-4 col-sm-12">
					<?php if ( get_field('logo', 'options') ) : ?>
						<div class="site-logo mb-3"><a href="<?php echo get_site_url(); ?>"><img src="<?php the_field('logo', 'options') ?>"></a></div>
					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
						<?php dynamic_sidebar( 'footer-1' ); ?>
					<?php endif; ?>
					
					<?php if (have_rows('drustvene_mreze','options')) : ?>	
						<div class="social-footer">
							<?php while (have_rows('drustvene_mreze','options')) : the_row(); ?>
								<a href="<?php the_sub_field('link'); ?>">
									<i class="icon-<?php the_sub_field('ikonica'); ?>"></i>
								</a>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
				<div class="col-xl-2 col-lg-4 col-md-4 col-sm-12">
					<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
						<?php dynamic_sidebar( 'footer-2' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-4 col-sm-12">
					<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
						<?php dynamic_sidebar( 'footer-3' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-xl-2 col-lg-4 col-md-3 col-sm-12">
					<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
						<?php dynamic_sidebar( 'footer-4' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-xl-2 col-lg-4 col-md-3 col-sm-12">
					<?php if ( is_active_sidebar( 'footer-5' ) ) : ?>
						<?php dynamic_sidebar( 'footer-5' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="container site-info d-flex justify-content-center medium-text p-3">
			<?php $blog_info = get_bloginfo( 'name' ); ?>
			<?php if ( ! empty( $blog_info ) ) : ?>
				<a class="site-name" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>,
			<?php endif; ?>
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentynineteen' ) ); ?>" class="imprint">
				<?php
				/* translators: %s: WordPress. */
				printf( __( 'Proudly powered by %s.', 'twentynineteen' ), 'WordPress' );
				?>
			</a>
			<?php
			if ( function_exists( 'the_privacy_policy_link' ) ) {
				the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
			}
			?>
			<?php if ( has_nav_menu( 'footer' ) ) : ?>
				<nav class="footer-navigation" aria-label="<?php esc_attr_e( 'Footer Menu', 'twentynineteen' ); ?>">
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'footer',
							'menu_class'     => 'footer-menu',
							'depth'          => 1,
						)
					);
					?>
				</nav><!-- .footer-navigation -->
			<?php endif; ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
