//#region Owl carousel
// Popular destination initialization
jQuery(document).ready(function ($) {

  // Mobile navigation
  var menu = new MmenuLight(
    document.querySelector('#menu'),
    'all'
  );
  var navigator = menu.navigation({
      // selectedClass: 'Selected',
      // slidingSubmenus: true,
      theme: 'light',
      title: 'Ponuda'
  });
  var drawer = menu.offcanvas({
      position: 'right'
  });
  //  Open the menu.
  document.querySelector('#nav-icon')
  .addEventListener('click', evnt => {
      evnt.preventDefault();
      drawer.open();
  });
  jQuery(".mob-menu-close").click(function() {
      drawer.close();
  });


  $('.owl-home-tab-1').owlCarousel({
    loop:false,
    margin:10,
    nav:false,
    dots:true,
    autoplay:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
})
  // Home slider initialization
  $(".main-home-slider").owlCarousel({
    loop: true,
    margin: 20,
    nav: false,
    responsive: {
      0: {
        items: 1,
        dots: true,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 1,
        dots: false,
      },
    },
  });
  $(".popular-destination-slider").owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    dots: true,
    responsive: {
      0: {
        items: 1,
        stagePadding: 50,
      },
      600: {
        items: 2,
      },
      1000: {
        items: 3,
      },
      1400: {
        items: 4,
      },
    },
  });
  // Hotel preview
  var sync1 = $("#sync1");
  var sync2 = $(".navigation-thumbs");
  var thumbnailItemClass = ".owl-item";
  var slides = sync1
    .owlCarousel({
      video: true,
      startPosition: 12,
      items: 1,
      loop: true,
      margin: 10,
      autoplay: false,
      nav: false,
      dots: false,
    })
    .on("changed.owl.carousel", syncPosition);
  function syncPosition(el) {
    $owl_slider = $(this).data("owl.carousel");
    var loop = $owl_slider.options.loop;
    if (loop) {
      var count = el.item.count - 1;
      var current = Math.round(el.item.index - el.item.count / 2 - 0.5);
      if (current < 0) {
        current = count;
      }
      if (current > count) {
        current = 0;
      }
    } else {
      var current = el.item.index;
    }
    var owl_thumbnail = sync2.data("owl.carousel");
    var itemClass = "." + owl_thumbnail.options.itemClass;
    var thumbnailCurrentItem = sync2
      .find(itemClass)
      .removeClass("synced")
      .eq(current);
    thumbnailCurrentItem.addClass("synced");
    if (!thumbnailCurrentItem.hasClass("active")) {
      var duration = 300;
      sync2.trigger("to.owl.carousel", [current, duration, true]);
    }
  }
  var thumbs = sync2
    .owlCarousel({
      startPosition: 12,
      loop: false,
      margin: 10,
      autoplay: false,
      nav: false,
      dots: false,
      responsive: {
        0: {
          items: 4,
        },
        600: {
          items: 4,
        },
        1000: {
          items: 4,
        },
        1400: {
          items: 6,
        },
      },
      onInitialized: function (e) {
        var thumbnailCurrentItem = $(e.target)
          .find(thumbnailItemClass)
          .eq(this._current);
        thumbnailCurrentItem.addClass("synced");
      },
    })
    .on("click", thumbnailItemClass, function (e) {
      e.preventDefault();
      var duration = 300;
      var itemIndex = $(e.target).parents(thumbnailItemClass).index();
      sync1.trigger("to.owl.carousel", [itemIndex, duration, true]);
    })
    .on("changed.owl.carousel", function (el) {
      var number = el.item.index;
      $owl_slider = sync1.data("owl.carousel");
      $owl_slider.to(number, 100, true);
    });
});
//#endregion
jQuery(document).ready(function ($) {

  // Single prikaz vile, oboji aktivni tab na mobile
  $('.expanded-tab').on('click',function(){
    $(this).toggleClass('active');
  })


  $(document).ready(function () {
    if (window.history.state != null) {
      $(".destinacije-listing").html(window.history.state);
      ///$('.malinky-load-more a').attr("href", url.href.slice(0, url.href.lastIndexOf('/') - 1) + (str + 1) + '/');
    }
    /*console.log(window.history.state);*/
  });
  let helpFunctions = {
    detectScrollDirection: function () {
      var st = $(this).scrollTop();
      if (st > lastScrollTop) {
        // Scroll top to bottom
        $(".second-menu-bg").addClass("scroll-top");
      } else {
        // Scroll bottom to top
        $(".second-menu-bg")
          .addClass("scroll-bottom")
          .removeClass("scroll-top");
      }
      lastScrollTop = st;
    },
    disableScroll: function () {
      // Get the current page scroll position
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      (scrollLeft = window.pageXOffset || document.documentElement.scrollLeft),
        // if any scroll is attempted, set this to the previous value
        (window.onscroll = function () {
          window.scrollTo(scrollLeft, scrollTop);
        });
    },
    enableScroll: function () {
      window.onscroll = function () {};
    },
  };
  //#region Menu
  // Detect scroll and hide/show secondary menu
  let lastScrollTop = 0;
  $(document).scroll(helpFunctions.detectScrollDirection);
  //#endregion Menu
  //#region Contact sidebar
  let open = false;
  function showSidebar(cardType,button,darkBg) {
    
    open = !open;
    if ($(this).hasClass(darkBg)) {
      helpFunctions.enableScroll();
      $(button).removeClass("move-left");
      $(button + ' i')
        .toggleClass("icon-arrow-right")
        .toggleClass("icon-telephone-bold");
    }
    $(cardType).toggleClass("contact-card-show");
    $(this)
      .find("i")
      .toggleClass("icon-arrow-right")
      .toggleClass("icon-telephone-bold");
    $(darkBg).toggleClass("d-block");
    if (open == true) {
      helpFunctions.disableScroll();
      $(this).addClass("move-left");
    } else if (open == false) {
      helpFunctions.enableScroll();
      $(this).removeClass("move-left");
    }
  }
  let contactSidebarButton = $("#contactSidebarButton");
  contactSidebarButton.on("click", showSidebar.bind(contactSidebarButton,'.contact-card','#contactSidebarButton','.focus-dark-bg'));
  let filterButton = $("#mobFilterButton");
  filterButton.on("click", showSidebar.bind(filterButton,'.mob-filter-card','#mobFilterButton','.focus-dark-bg-filters'));
  $(".focus-dark-bg").on("click", showSidebar.bind(contactSidebarButton,'.contact-card','#contactSidebarButton','.focus-dark-bg'));
  $(".focus-dark-bg-filters").on("click", showSidebar.bind(filterButton,'.mob-filter-card','#mobFilterButton','.focus-dark-bg-filters'));
  //#endregion Contact sidebar


  //#region Global initialization
  // Tooltips
  $(function () {
    $('[data-toggle="tooltip"]').tooltip();
  });
  //#endregion
  //#region Expandable text
  $("#expandable .expand-btn").on("click", function () {
    $("#expandable .card-header").toggleClass("expand");
    if ($(this).text() == "Prikaži više") {
      $(this).text("Sakrij");
    } else {
      $(this).text("Prikaži više");
    }
  });
  //#endregion Expandable text
  //#region Show contact card
  $(".inquiry-btn").on("click", function () {
    $(".inquiry-card").toggleClass("show");
  });
  //#endregion Show contact card
  particlesJS("particles-js", {
    particles: {
      number: {
        value: 400,
        density: {
          enable: true,
          value_area: 3000,
        },
      },
      color: {
        value: "#ffffff",
      },
      shape: {
        type: "image",
        stroke: {
          width: 3,
          color: "#fff",
        },
        polygon: {
          nb_sides: 5,
        },
        image: {
          src:
            "http://www.dynamicdigital.us/wp-content/uploads/2013/02/starburst_white_300_drop_2.png",
          width: 100,
          height: 100,
        },
      },
      opacity: {
        value: 0.7,
        random: false,
        anim: {
          enable: false,
          speed: 1,
          opacity_min: 0.1,
          sync: false,
        },
      },
      size: {
        value: 5,
        random: true,
        anim: {
          enable: false,
          speed: 20,
          size_min: 0.1,
          sync: false,
        },
      },
      line_linked: {
        enable: false,
        distance: 50,
        color: "#ffffff",
        opacity: 0.6,
        width: 1,
      },
      move: {
        enable: true,
        speed: 5,
        direction: "bottom",
        random: true,
        straight: false,
        out_mode: "out",
        bounce: false,
        attract: {
          enable: true,
          rotateX: 300,
          rotateY: 1200,
        },
      },
    },
    interactivity: {
      detect_on: "canvas",
      events: {
        onhover: {
          enable: true,
          mode: "bubble",
        },
        onclick: {
          enable: true,
          mode: "repulse",
        },
        resize: true,
      },
      modes: {
        grab: {
          distance: 150,
          line_linked: {
            opacity: 1,
          },
        },
        bubble: {
          distance: 200,
          size: 20,
          duration: 2,
          opacity: 8,
          speed: 3,
        },
        repulse: {
          distance: 200,
          duration: 0.2,
        },
        push: {
          particles_nb: 4,
        },
        remove: {
          particles_nb: 2,
        },
      },
    },
    retina_detect: true,
  });
});
