<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main pt-5 avio-karte">

		<?php while (have_posts()) : the_post(); ?>


			<?php $sezona =  get_field('izaberi_sezonu'); ?>

			<div class="header-image avio-karte" style="background-image: url('<?php the_field($sezona, 'option'); ?>')">
				<div class="container">
					<div class="row">
						<div class="col-12 d-flex justify-content-between align-items-center top-head-title">

							<div class="d-flex align-items-start hotel-title">
								<h2 class="mb-0 d-flex flex-column">
									<?php the_title();  ?>
								</h2>


							</div>
							<div class="d-flex align-items-center hotel-share">
								<span>Podeli: </span>
								<?php require('templates/function/share-horizontal.php');
								?>
							</div>

						</div>
						<div class="col-xl-7 col-lg-6 col-sm-12">

							<?php
							$slika = get_field('galerija');
							$size = 'full'; // (thumbnail, medium, large, full or custom size)
							?>
							<?php if (get_field('galerija')) : ?>
								<div id="sync1" class="slider owl-carousel mb-3">
									<?php if ($slika) : ?>
										<?php foreach ($slika as $image_id) : ?>
											<div class="item">
												<?php echo wp_get_attachment_image($image_id, $size, ["class" => "object-fit-center border-radius-mobile"]); ?>
											</div>
										<?php endforeach; ?>
									<?php endif; ?>
								</div>
								<div id="sync2" class="navigation-thumbs owl-carousel">
									<?php if ($slika) : ?>
										<?php foreach ($slika as $image_id) : ?>
											<div class="item">
												<?php echo wp_get_attachment_image($image_id, $size, ["class" => "object-fit-center border-radius-mobile"]); ?>
											</div>
										<?php endforeach; ?>
									<?php endif; ?>
								</div>
							<?php else : ?>
								<div style="background-image:url('<?php echo get_stylesheet_directory_uri(); ?>/img/noimg.png')" class="no-img"></div>
							<?php endif; ?>
						</div>
						<div class="col-xl-5 col-lg-6 col-sm-12">
							<div class="info-card d-flex justify-content-center flex-column align-items-center">
								<div class="logo-kompanije">
									<?php
									$image = get_field('logo_kompanije');
									$size = 'medium'; // (thumbnail, medium, large, full or custom size)
									if ($image) {
										echo wp_get_attachment_image($image, $size);
									}
									?>
								</div>
								<?php if (have_rows('let')) : ?>
									<?php while (have_rows('let')) : the_row(); ?>
										<div class="let">
											<div class="od-do">
												<h2><?php the_field('od_destinacija'); ?></h2>
												<div class="airport">
													<i class="icon-surface1 main-color-text"></i>
												</div>
												<h2><?php the_field('do_destinacija'); ?></h2>
											</div>
											<div class="let-info">
												<section class="d-flex flex-column">
													<span>Dan(i)</span>
													<span class="font-weight-bold"><?php the_sub_field('dan'); ?></span>
												</section>
												<section class="d-flex flex-column">
													<span>Polazak</span>
													<span class="font-weight-bold"><?php the_sub_field('polazak'); ?></span>
												</section>
												<section class="d-flex flex-column">
													<span>Dolazak</span>
													<span class="font-weight-bold"><?php the_sub_field('dolazak'); ?></span>
												</section>
												<section class="d-flex flex-column w-cena">
													<span>Cena</span>
													<span class="font-weight-bold"><?php the_sub_field('cena'); ?></span>
												</section>
											</div>
											<?php if (get_sub_field('povratni_let') == true) : ?>
												<div class="od-do">
													<h2><?php the_field('do_destinacija'); ?></h2>
													<div class="airport">
														<i class="icon-surface1 main-color-text"></i>
													</div>
													<h2><?php the_field('od_destinacija'); ?></h2>
												</div>
												<div class="let-info">
													<section class="d-flex flex-column">
														<span>Dan(i)</span>
														<span class="font-weight-bold"><?php the_sub_field('dan_povratnog_leta'); ?></span>
													</section>
													<section class="d-flex flex-column">
														<span>Polazak</span>
														<span class="font-weight-bold"><?php the_sub_field('polazak_povratni'); ?></span>
													</section>
													<section class="d-flex flex-column">
														<span>Dolazak</span>
														<span class="font-weight-bold"><?php the_sub_field('dolazak_povratni'); ?></span>
													</section>
													<section class="d-flex flex-column w-cena">
														<!-- <span>Cena</span> -->
														<!-- <span class="font-weight-bold"><?php the_sub_field('cena'); ?></span> -->
													</section>
												</div>
											<?php endif; ?>
										</div>
									<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container mt-5 mb-5">
				<div class="row">
					<div class="col-xl-9 col-lg-8 col-sm-12">
						<div class="hotel-tabs">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true"><i class="icon-information"></i> INFORMACIJE</a>
								</li>
							</ul>
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">

									<div class="d-flex justify-content-between align-items-center info-tab-top">
										<?php
										$tipPrevoza = get_field('tip_prevoza');
										if ($tipPrevoza) : ?>
											<div class="tip-prevoza">
												<?php foreach ($tipPrevoza  as $prevoz) : ?>
													<div class="icon">
														<i class="icon-<?php echo $prevoz['value']; ?>"></i>
														<span><?php echo $prevoz['label']; ?></span>
													</div>
												<?php endforeach; ?>
											</div>
										<?php endif; ?>

										<?php
										$uslugaHotela = get_field('usluga_hotela');
										if ($uslugaHotela) : ?>
											<div class="usluga-hotela">
												<?php foreach ($uslugaHotela  as $usluga) : ?>
													<div class="icon">
														<span data-toggle="tooltip" data-placement="top" title="Tooltip on top"><?php echo $usluga['label']; ?></span>
													</div>
												<?php endforeach; ?>
											</div>
										<?php endif; ?>
									</div>


									<?php the_content(); ?>

								</div>

							</div>
							<div class="tab-pane">
								<div id="includesOrNot">
									<?php if (get_field('obuhvata')) : ?>
										<div class="item">
											<div class="card-header" id="headingObuhvata" data-toggle="collapse" data-target="#collapseObuhvata" aria-expanded="true" aria-controls="collapseObuhvata">
												<h5 class="mb-0 d-flex align-items-center justify-content-between">
													<button class="btn btn-link">
														AranŽman obuhvata:
													</button>
													<i class="icon-arrow-down"></i>
												</h5>
											</div>

											<div id="collapseObuhvata" class="collapse " aria-labelledby="headingObuhvata" data-parent="#includesOrNot">
												<div class="card-body">
													<?php the_field('obuhvata') ?>
												</div>
											</div>
										</div>
									<?php endif; ?>
									<?php if (get_field('neobuhvata')) : ?>
										<div class="item">
											<div class="card-header" id="headingNeObuhvata" data-toggle="collapse" data-target="#collapseNeObuhvata" aria-expanded="false" aria-controls="collapseNeObuhvata">
												<h5 class="mb-0 d-flex align-items-center justify-content-between">
													<button class="btn btn-link collapsed">
														AranŽman NE obuhvata:
													</button>
													<i class="icon-arrow-down"></i>
												</h5>
											</div>
											<div id="collapseNeObuhvata" class="collapse" aria-labelledby="headingNeObuhvata" data-parent="#includesOrNot">
												<div class="card-body">
													<?php the_field('neobuhvata') ?>
												</div>
											</div>
										</div>
									<?php endif; ?>
								</div>

								<!-- Napomena -->
								<?php if (get_field('tekst_napomene', 'option')) : ?>
									<div id="warningAccordion">
										<div class="item">
											<div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
												<h5 class="mb-0 d-flex align-items-center justify-content-between">
													<button class="btn btn-link">
														Napomene:
													</button>
													<i class="icon-arrow-down"></i>
												</h5>
											</div>

											<div id="collapseThree" class="collapse " aria-labelledby="headingThree" data-parent="#warningAccordion">
												<div class="card-body">
													<?php the_field('tekst_napomene', 'option') ?>
												</div>
											</div>
										</div>
									</div>
								<?php endif; ?>
							</div>

						</div>


					</div>
				</div>
			</div>

		<?php endwhile; // End the loop.
		?>


	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
?>