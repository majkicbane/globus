<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

		<div class="navs">
			<header id="masthead" class="site-header">

			<div class="container">

				<?php if ( get_field('logo', 'options') ) : ?>
					<div class="site-logo d-inline-block"><a href="<?php echo get_site_url(); ?>"><img src="<?php the_field('logo', 'options') ?>"></a></div>
				<?php endif; ?>

				<?php ubermenu( 'main' ); ?>
			</div><!-- .site-branding-container -->

				
			</header><!-- #masthead -->

			<div class="mob-menu-header">
				<nav id="menu">
					<div class="mob-menu-close close-right-top">
						<span class="icon-close"></span>
					</div>
					<?php wp_nav_menu( array( 'menu' => 'Primary' ) ); ?>
				</nav>
			</div>

			<i class="fa fa-bars" id="nav-icon"></i>

			<div class="second-menu-bg" id="bottommenu">
				<div class="container">
				<?php if ( is_active_sidebar( 'second-navbar' ) ) : ?>
					<div class="d-flex justify-content-center">
						<?php dynamic_sidebar( 'second-navbar' ); ?>
					</div>
				<?php endif; ?>
				</div>	
			</div>
		</div>
		

	<main id="content" class="site-content">
