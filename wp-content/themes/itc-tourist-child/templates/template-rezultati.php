<?php
/**
 * Template Name: Rezultati pretrage
 *
 * @package Threshtech
 */
get_header(); 
require_once('function/search-get.php');
?>
<div id="primary">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="post" id="post-<?php the_ID(); ?>">
        <?php 
  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
//   var_dump( $dodaci);
            if(!empty($destinacija) || (empty($drzava) && empty($destinacija) ) ) {
                //Ako je prosledio destinaciju
                $parent = $destinacija;
                // Ako je drzava ili oblast
                if(get_page_template_slug( $parent ) == 'templates/template-drzave.php'){
                    
                    //Uzima id od stranice kako bi vratio sve pod oblasti npr, Tasos - Potos, itd.
                    $idChildStranica = array();
                    $getChilds = array(
                        'post_type'      => 'page',
                        'posts_per_page' => -1,
                        'post_parent'    => $parent,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order'
                    );
                    
                    
                    $parentPage = new WP_Query( $getChilds );
                    
                    if ( $parentPage->have_posts() ) :
                    
                        while ( $parentPage->have_posts() ) : $parentPage->the_post();
                    
                            $idChildStranica[] = get_the_ID();
                    
                        endwhile; 
                    
                    endif; wp_reset_postdata(); 
                   
                    $rez = (array(
                        'post_type' => array('page'),
                        'order' => 'ASC',
                        'post_parent__in' => $idChildStranica,
                        'orderby' => 'title',
                        'meta_query'     => array(
                            array(
                                'key'   => '_wp_page_template',
                                'value' => array(
                                    'templates/template-vile.php',
                                    'templates/template-specijal.php',
                                )
                            )
                        )
                    ));
                    $rez = new WP_Query($rez);
                }else{ //Ako je destinacija
                    $rez = (array(
                        'post_type' => array('page'),
                        'order' => 'ASC',
                        'post_parent' => $parent,
                        'orderby' => 'title',
                        'meta_query'     => array(
                            array(
                                'key'   => '_wp_page_template',
                                'value' => array(
                                    'templates/template-vile.php',
                                    'templates/template-specijal.php',
                                )
                            )
                        )
                    ));
                    $rez = new WP_Query($rez);
                }
                

            } else {

                //Ako je prosledio samo drzavu
                $parent = $drzava;
                
                $args = array(
                    'post_type' => array('page'),
                        'order' => 'ASC',
                        'orderby' => 'title',
                        'meta_query'     => array(
                            array(
                                'key'   => '_wp_page_template',
                                'value' => array(
                                    'templates/template-vile.php',
                                    'templates/template-specijal.php',
                                )
                            )
                        )
                );
                $pages = get_pages($args);
            if(!empty($pages)) {

                if ($pages) {
                    foreach ($pages as $page) :
                    $drzave[] = $page->ID;
                   endforeach; 
                }

                $rez = array(
                    'post_type' => 'page',
                    'post_parent__in' => $drzave,
                    'posts_per_page' => 12,
                    'meta_query' => $dodaci,
                    'order' => $order,
                    'orderby' => $orderby,
                    'meta_key' => $sort,
                    'paged' => $paged,
                        );
                    $rez = new WP_Query($rez);

                }
                else {
                    // Ubacuje sve pod straniceu niz $svePodStranice koji prosledjujemo u query
                    $podStranice = get_pages( array( 'child_of' => $parent ) );
                    foreach ($podStranice as $stranica) :
                        $svePodStranice[] = $stranica->ID;
                    endforeach; 

                    $rez = (array(
                        'post_type'=>'page',
                        'posts_per_page' => 12,
                        'post_parent__in' => $svePodStranice,
                        'meta_query'     => array(
                            array(
                                'key'   => '_wp_page_template',
                                'value' => array(
                                    'templates/template-vile.php',
                                    'templates/template-specijal.php',
                                )
                            )
                        )
                        ));
                    $rez = new WP_Query($rez);
                }


                }
                // Ako je izabrao samo sezonu
                if( empty($drzava) && empty($destinacija) ){

                    $parent = $sezona;

                    $podStranice = get_pages( array( 'child_of' => $parent ) );
                    foreach ($podStranice as $stranica) :
                        $svePodStranice[] = $stranica->ID;
                    endforeach; 

                    $rez = (array(
                        'post_type'=>'page',
                        'posts_per_page' => 12,
                        'post_parent__in' => $svePodStranice,
                        'meta_query'     => array(
                            array(
                                'key'   => '_wp_page_template',
                                'value' => array(
                                    'templates/template-vile.php',
                                    'templates/template-specijal.php',
                                )
                            ),$dodaci
                        )
                        ));
                    $rez = new WP_Query($rez);
                }
            ?>


        <!-- SANDBOX -->
        <?php  
                    // if($rez->have_posts()) {
                    // echo 'IMA ---------------------'.'<br>';
                    // while ($rez->have_posts()) {
                    //     $rez->the_post();
            
                    //     echo get_the_title(get_the_ID()) .'<br>';
                    // }
                    // wp_reset_postdata();
                    // }else{
                    //     echo 'NEMA ----------------------';
                    // } 
                    ?>
        <!-- SANDBOX END -->


    </div>
    <?php endwhile; endif; ?>
</div><!-- #primary -->

<!-- NOVO ========================================================================================================= -->


<?php
global $post;
$currentID = $post->ID;
$pages_no = get_pages("child_of=$currentID&depth=1&parent=$currentID");
$count = count($pages_no);
?>
<div id="header-image" class="rezultat-pretrage-header">
    <?php if (get_field('header_image')) : ?>
    <img src="<?php the_field('header_image'); ?>" class="object-fit-center" />
    <?php $noHeaderClass = '' ?>
    <?php else : ?>
    <img src="<?php the_field('pozadina_drzavadestinacija', 'option'); ?>" class="object-fit-center" />
    <?php $noHeaderClass = 'no-header-class' ?>
    <?php endif; ?>
    <div class="white-bg filter-result-bg"></div>
    <div class="container position-absolute d-flex flex-column justify-content-between">
        <div class="d-flex flex-column title">
            <?php echo '<h1 class="entry-title text-uppercase ' . $noHeaderClass . '">Rezultati pretrage</h1>' ?>
        </div>

        <div class="destinacije">
            <?php  if($rez->have_posts()) : ?>
            <?php $results = $rez->found_posts;  ?>

            <div class="destinacije-naslov">
                <?php if($results == 1) : ?>
                <p class="br-destinacija"><span class="icon-magnifier2"></span> Pronadjen je <?php echo $results;?>
                    rezultat koji zadovoljava kriterijume pretrage.</p>
                <?php elseif($results > 1 && $results < 5) : ?>
                <p class="br-destinacija"><span class="icon-magnifier2"></span> Pronadjena su <?php echo $results;?>
                    rezultata koja zadovoljavaju kriterijume pretrage.</p>
                <?php elseif($results >= 5) : ?>
                <p class="br-destinacija"><span class="icon-magnifier2"></span> Pronadjeno je <?php echo $results;?>
                    rezultata koji zadovoljavaju kriterijume pretrage.</p>
                <?php endif; ?>
            </div>
            <div class="destinacije-listing">
                <?php while ($rez->have_posts()) : $rez->the_post(); ?>
                <?php get_template_part( 'content', 'rezultati' ); ?>
                <?php endwhile; ?>

                <?php if ($rez->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                <nav class="prev-next-posts">
                    <div class="prev-posts-link">
                        <?php echo get_next_posts_link( 'Older Entries', $rez->max_num_pages ); // display older posts link ?>
                    </div>
                    <div class="next-posts-link">
                        <?php echo get_previous_posts_link( 'Newer Entries' ); // display newer posts link ?>
                    </div>
                </nav>
                <?php } ?>
            </div>
        </div>
        <?php else : ?>
            <?php
                echo '<p class="br-destinacija"><span class="icon-magnifier2"></span> Nije pronadjen ni jedan
                rezultat koji zadovoljava kriterijume pretrage.</p>';

                if($tip_smestaja == 'apartman'){
                    $nemaRezultataPoruka = '<p>Žao nam je, ali ni jedan <span class="nofount-label">apartman</span> ne ispunjava
                    kriterijume Vaše pretrage.<br>
                    Pokušajte ponovo sa drugim kriterijumima, ili pretražite hotele.</p>';
                }else if($tip_smestaja == 'hotel'){
                    $nemaRezultataPoruka = '<p>Žao nam je, ali ni jedan <span class="nofount-label">hotel</span> ne ispunjava kriterijume
                    Vaše pretrage.<br>
                    Pokušajte ponovo sa drugim kriterijumima, ili pretražite apartmane.</p></p>';
                }
            ?>
        
    </div>
    <?php endif; ?>

</div>
</div>


<div class="container mb-5">
    <div class="row">
        <div id="primary" class="col-xl-9 col-lg-8 col-sm-12">
            <?php echo $nemaRezultataPoruka ?>
            <form method="get" id="destinacije-forma" class="filteri" action="<?php echo $current_url; ?>/">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php $parent = $post->ID; ?>
                <?php endwhile;
					endif; ?>
                <?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$dest = $rez;
					?>
                <?php if ($dest->have_posts()) : ?>
                <div class="page destinacije-wrapper-b" id="p<?php echo $paged; ?>">
                    <div class="row destinacije destinacije-listing">
                        <?php while ($dest->have_posts()) : $dest->the_post(); ?>
                        <?php if(wp_is_mobile()) : ?>
                        <?php require('function/prikaz-objekata-mob.php'); ?>
                        <?php else : ?>
                        <?php require('function/prikaz-objekata.php'); ?>
                        <?php endif; ?>
                        <?php endwhile;
								wp_reset_postdata(); ?>
                        <?php if ($dest->max_num_pages > 1) { // check if the max number of pages is greater than 1  
								?>
                        <nav class="prev-next-posts">
                            <div class="prev-posts-link">
                                <?php echo get_next_posts_link('Older Entries', $dest->max_num_pages); // display older posts link 
											?>
                            </div>
                            <div class="next-posts-link">
                                <?php echo get_previous_posts_link('Newer Entries'); // display newer posts link 
											?>
                            </div>
                        </nav>
                        <?php } ?>
                    </div>
                </div>
                <?php endif; ?>
            </form>
        </div><!-- #primary -->
        <?php if(!(wp_is_mobile())) : ?>
        <div class="col-xl-3 col-lg-4 col-sm-12">
            <form method="get" id="filter-forma" class="filteri"
                action="<?php echo esc_url( home_url( '/' ) ); ?>/pretraga/">

                <!-- <div class="sortiranje-rezultata">
                    <?php require_once('function/results-sort.php'); ?>
                </div> -->
                <aside id="secondary">
                    <div class="filter-box filter-box-side">
                        <?php 
                            require('function/filter-search-results.php'); 
                        ?>
                        <div class="ucitavam"></div>
                    </div>
                </aside>
            </form>
        </div>
        <?php endif; ?>


        <!-- Mobile filters (START) -->
		<div id="mobFilterButton">
			<i class="icon-search"></i>
		</div>
		<div class="focus-dark-bg-filters"></div>
		<div class="mob-filter-card">
			<div class="filter-box">
            <form method="get" id="filter-forma" class="filteri"
                action="<?php echo esc_url( home_url( '/' ) ); ?>/pretraga/">
                <?php 
                    require('function/filter-search-results.php'); 
                ?>
            </form>
			</div>
		</div>
		<!-- Mobile filters (END) -->


    </div>
</div> <!-- container -->

<?php get_footer(); ?>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>