<?php

/**
 * Template Name: 1. dubina - Sezona
 *
 * @package TA Pluton
 */
get_header(); ?>
<!-- Ako ima header slika -->

<?php
global $post;
$currentID = $post->ID;
$pages_no = get_pages("child_of=$currentID&depth=1&parent=$currentID");
$count = count($pages_no); ?>

<div id="header-image">
	<?php if (get_field('header_image')) : ?>
		<img src="<?php the_field('header_image'); ?>" class="object-fit-center" />
		<?php $noHeaderClass = '' ?>
	<?php else : ?>
		<img src="<?php the_field('pozadina_drzavadestinacija', 'option'); ?>" class="object-fit-center" />
		<?php $noHeaderClass = 'no-header-class' ?>
	<?php endif; ?>
	<div class="white-bg"></div>

	<div class="container position-absolute d-flex justify-content-between">

		<div class="d-flex flex-column title">
			<?php
			if ($count == 1) {
				the_title('<h1 class="entry-title ' . $noHeaderClass . '">', '</h1> <span class="br-destinacija">(' . $count . ' država u ponudi)</span>');
			} else if ($count > 1 && $count < 5) {
				the_title('<h1 class="entry-title ' . $noHeaderClass . '">', '</h1> <span class="br-destinacija">(' . $count . ' države u ponudi)</span>');
			} else {
				the_title('<h1 class="entry-title ' . $noHeaderClass . '">', '</h1> <span class="br-destinacija">(' . $count . ' država u ponudi)</span>');
			} ?>
			<?php if (function_exists('yoast_breadcrumb')) {
				yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
			}
			?>
		</div>

		<?php if (get_field('embed_video')) : ?>
			<div class="video-container d-flex justify-content-center align-items-center <?php echo $noHeaderClass; ?>" data-toggle="modal" data-target="#openVideo">
				<div class="icon-container">
					<div class="pulse"></div>
					<div class="icon-border <?php echo $noHeaderClass; ?>">
						<div class="icon <?php echo $noHeaderClass; ?>">
							<i class="icon-play <?php echo $noHeaderClass; ?>"></i>
						</div>
					</div>
				</div>
				<span class="ml-3">Pogledaj video</span>

				<!-- Modal -->
				<div class="modal fade" id="openVideo" tabindex="-1" role="dialog" aria-labelledby="openVideoTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="openVideoTitle"><?php the_field('naslov_video_klipa') ?></h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php the_field('embed_video') ?>?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
							</div>
						</div>
					</div>
				</div>

			</div>
		<?php endif; ?>

	</div>


</div>

<div class="main-content">
	<div class="container">
		<div class="row">
			<div class="col-lg-9">

				<div id="expandable">
					<div class="card-header" id="headingOne">
						<p><?php the_field('kraci_opis') ?></p>
					</div>
					<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#expandable">
						<div class="card-body">
							<?php while (have_posts()) : the_post(); ?>
								<?php get_template_part('template-parts/content/content', 'page'); ?>
								<?php $parent = $post->ID; ?>
							<?php endwhile; // end of the loop. 
							?>
						</div>
					</div>
					<span class="expand-btn mt-2 mb-4" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Prikaži više</span>
				</div>

				<?php $dest = (array(
					'post_type' => 'page',
					'orderby' => 'menu_order',
					'order' => 'ASC',
					'post_parent' => $parent,
					'posts_per_page' => 9,
					'paged' => $paged,
				));
				$dest = new WP_Query($dest);
				?>

				<?php if ($dest->have_posts()) : ?>

					<article class="destinacije-wrapper mb-5">

						<h3 class="text-uppercase font-weight-bold mb-4"><?php the_title(); ?> - Destinacije u ponudi:</h3>

						<div class="row destinacije destinacije-listing">
							<?php while ($dest->have_posts()) : $dest->the_post(); ?>
								<div class="col-xl-4 col-lg-4 col-md-6 posebno hover1">
									<a href="<?php the_permalink(); ?>" class="single-destinacija-drzave">
										<figure>
											<a href="<?php the_permalink() ?>">
												<?php the_post_thumbnail('ponude-img', array('class' => 'object-fit-center')); ?>
											</a>
										</figure>

										<h5><?php the_title(); ?></h5>
									</a>
								</div>
							<?php endwhile;
							wp_reset_postdata(); ?>

							<?php if ($dest->max_num_pages > 1) : // check if the max number of pages is greater than 1  
							?>
								<nav class="prev-next-posts">
									<div class="prev-posts-link">
										<?php echo get_next_posts_link('Older Entries', $dest->max_num_pages); // display older posts link 
										?>
									</div>
									<div class="next-posts-link">
										<?php echo get_previous_posts_link('Newer Entries'); // display newer posts link 
										?>
									</div>
								</nav>
							<?php endif; ?>
						</div>

					</article>

				<?php endif; ?>

			</div>

			<div class="col-lg-3">
			
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>