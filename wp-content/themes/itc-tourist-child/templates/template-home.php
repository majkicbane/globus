<?php
/**
 * Template Name: Početna
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>
<?php
function clean($string)
{
	$string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
	return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <?php if (get_field('top_section') == 'Boxes') {
			$boxStyle = get_field('boxes_style');
		} else {
			$sliderStyle = get_field('slider_style');
		}
		?>
        <!-- Main slider (START) -->
        <div class="slider d-flex flex-column align-items-center">
            <?php if (have_rows('slider')) : ?>
            <div class="owl-carousel main-home-slider owl-theme">
                <?php while (have_rows('slider')) : the_row(); ?>
                <div>
                    <div class="slider-item temp-bg" style="background-image: url('<?php the_sub_field('image') ?>');">
                        <div
                            class="container d-flex flex-column align-items-<?php the_sub_field('content_position') ?> mobile-hide">
                            <span class="subtitle"
                                style="background-color:<?php the_sub_field('boja_dugmeta_i_bedza'); ?>!important;"><?php the_sub_field('slider_subtitle'); ?></span>
                            <span class="content" style="color:<?php the_sub_field('boja_teksta'); ?>!important;">
                                <?php the_sub_field('content'); ?>
                            </span>
                            <a href="" class="primary-btn primary-full"
                                style="background-color:<?php the_sub_field('boja_dugmeta_i_bedza'); ?>!important;"><?php the_sub_field('button_text'); ?>
                                <i class="icon-arrow-right-2 ml-2"></i></a>
                        </div>
                    </div>
                    <div class="container flex-column all-items">
                        <span class="subtitle"><?php the_sub_field('slider_subtitle'); ?></span>
                        <span class="content">
                            <?php the_sub_field('content'); ?>
                        </span>
                        <a href="" class="primary-btn"
                            style="background-color:<?php the_sub_field('boja_dugmeta_i_bedza'); ?>!important;"><?php the_sub_field('button_text'); ?>
                            <i class="icon-arrow-right-2 ml-2"></i></a>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
            <!-- Filters (START) -->
            <div class="filter-box">

                <div id="apartmaniFilter">
                    <?php 
                    if(!(wp_is_mobile())){
                        require('function/filter-apartmani.php'); 
					}
                    ?>
                    <div class="ucitavam"></div>
                </div>
            </div>
            <!-- Filters (END) -->
        </div>
        <!-- Main slider (END) -->
        <?php /*if ($boxStyle) : ?>
        <div class="container">
            <?php if (have_rows('home_banners')) : ?>
            <div class="ponude-grid grid-<?php echo $boxStyle; ?>">
                <?php $i = 1;
						while (have_rows('home_banners')) : the_row();
							$boxPonuda = get_sub_field('ponuda');
							$label1 = get_sub_field('text_label_1');
							$label2 = get_sub_field('text_label_2');
						?>
                <?php if ($boxPonuda) : ?>
                <div class="grid-<?php echo $i; ?>">
                    <a href="<?php the_permalink($boxPonuda); ?>">
                        <div class="single-ponuda label-top">
                            <?php if (has_post_thumbnail($boxPonuda)) {
												echo get_the_post_thumbnail($boxPonuda, 'ponude-img', array('class' => 'object-fit-center'));
											} ?>
                            <?php if ($label1) : ?>
                            <span class="box-badge"><?php echo $label1; ?></span>
                            <?php endif; ?>
                            <?php if ($label2) : ?>
                            <span class="box-promo-text"><?php echo $label2; ?></span>
                            <?php endif; ?>
                        </div>
                    </a>
                </div>
                <?php if ($boxStyle == 'style_2' || $boxStyle == 'style_3' || $boxStyle == 'style_4') : ?>
                <?php if ($i == 5) {
										break;
									}  ?>
                <?php endif; ?>
                <?php endif; ?>
                <?php $i++;
						endwhile; ?>
            </div>
            <?php endif; ?>
        </div>
        <?php elseif ($sliderStyle) : ?>
        <?php endif; */?>
        <div class="top-background" style="background-image: url('<?php the_field('top_bg') ?>');">
            <!-- Tabs & banners (START) -->
            <?php if (have_rows('homepage_tabs')) : ?>
            <div class="container homepage-tabs">
                <div class="row">
                    <div class="col-xl-9 col-lg-7 custom-pr">
                        <?php if (have_rows('baneri')) : ?>
                        <div class="banner-sm-section d-flex justify-content-between mb-5">
                            <?php $j = 1; ?>
                            <?php while (have_rows('baneri')) : the_row(); ?>
                            <a href="<?php the_sub_field('page_link'); ?>"
                                class="w-100 m-mobile-none <?php if ($j % 2 == 0) : ?>mr-0<?php else : ?> mr-3 <?php endif; ?>">
                                <div class="banner-sm mr-2"
                                    style="background-image: url('<?php the_sub_field('slika_banera') ?>');">
                                    <div class="blue-bg"></div>
                                    <h3 class="z-10"><?php the_sub_field('naslov_banera'); ?></h3>
                                    <h4 class="z-10"><?php the_sub_field('podnaslov_banera'); ?></h4>
                                </div>
                            </a>
                            <?php $j++;
									endwhile; ?>
                        </div>
                        <?php endif;
							wp_reset_postdata(); ?>
                        <div class="home-tabs-wrapper">
                            <?php if (get_field('homepage_tabs_title')) : ?>
                            <h3 class="tabs-section-title mb-4 font-weight-medium">
                                <?php the_field('homepage_tabs_title'); ?></h3>
                            <?php endif; ?>
                            <div class="tabs-header d-flex justify-content-between align-items-center">
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <?php $tab = 1;
										while (have_rows('homepage_tabs')) : the_row();
											$tabTitle = get_sub_field('tab_title');
										?>
                                    <li class="nav-item">
                                        <a class="nav-link <?php if ($tab == 1) : ?>active<?php endif; ?>"
                                            id="tab-home-<?php echo $tab; ?>" data-toggle="pill"
                                            href="#homepage-tab-<?php echo $tab; ?>" role="tab"
                                            aria-controls="pills-home" aria-selected="true"><?php echo $tabTitle; ?></a>
                                    </li>
                                    <?php $tab++;
										endwhile; ?>
                                </ul>
                            </div>
                            <div class="tab-content" id="pills-tabContent">
                                <?php $tabContent = 1;
									while (have_rows('homepage_tabs')) : the_row(); ?>
                                <div class="tab-pane fade <?php if ($tabContent == 1) : ?>show active<?php endif; ?>"
                                    id="homepage-tab-<?php echo $tabContent; ?>" role="tabpanel"
                                    aria-labelledby="tab-home-<?php echo $tabContent; ?>">
                                    <?php if (have_rows('offers')) : ?>
                                    <div class="row destinacije-tab">
                                        <div class="owl-carousel owl-home-tab-1 owl-theme">
                                            <?php while (have_rows('offers')) : the_row();
														$destination = get_sub_field('select_destination');
														$customTitle = get_sub_field('title');
														$promoText = get_sub_field('promo_text');
														$badge = get_sub_field('badge');
													?>
                                            <div class="item">
                                                <a href="<?php the_permalink($destination); ?>">
                                                    <div class="single-destinacija-tab">
                                                        <figure class="ponuda-img-wrapper">
                                                            <div class="black-bg"></div>
                                                            <a href="<?php the_permalink($destination); ?>">
                                                                <?php if (has_post_thumbnail($destination)) {
																				echo get_the_post_thumbnail($destination, 'ponude-img', array('class' => 'object-fit-center'));
																			} ?>
                                                            </a>
                                                            <?php if ($badge) : ?>
                                                            <span class="ponuda-badge angle-bg"></span>

                                                            <span
                                                                class="ponuda-badge angle-text"><?php echo $badge; ?></span>
                                                            <!-- <div class="angle-badge">
																				<span><?php echo $badge; ?></span>
																			</div> -->
                                                            <?php endif; ?>
                                                            <?php if ($promoText) : ?>
                                                            <span class="ponuda-promo-text"><a
                                                                    href="<?php the_permalink($destination); ?>"><?php echo $customTitle; ?></a>
                                                                » <?php echo $promoText; ?></span>
                                                            <?php endif; ?>
                                                        </figure>
                                                        <h3></h3>
                                                    </div>
                                                </a>
                                            </div>

                                            <?php endwhile; ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <?php $tabContent++;
									endwhile; ?>
                            </div>
                        </div>
                    </div>
                    <?php if (have_rows('polasci')) : ?>
                    <div class="col-xl-3 col-lg-5">
                        <div class="side-card">
                            <div class="title">
                                <h5><?php the_field('sidebar_title') ?></h5>
                            </div>
                            <div class="items">
                                <?php while (have_rows('polasci')) : the_row(); ?>
                                <?php $featured_post = get_sub_field('grad'); ?>
                                <a href="<?php the_permalink($featured_post); ?>"
                                    class="single-item d-flex flex-column justify-content-between">
                                    <div class="top d-flex justify-content-between">
                                        <div class="d-flex align-items-center">
                                            <h6 class="d-inline-block mr-1 text-uppercase">
                                                <?php echo get_the_title($featured_post); ?></h6>
                                            <span class="badge small-text"><?php the_sub_field('red_bedge'); ?></span>
                                        </div>
                                        <h6 class="d-flex align-items-center main-color-text">
                                            <!-- <i class="icon-bus"></i> -->
                                            <?php if (get_field('tip_prevoza_spec', $featured_post)) : ?>
                                            <span class="small-badge small-badge-left d-flex ">
                                                <?php
																$tipPrevoza = get_field('tip_prevoza_spec', $featured_post);
																if ($tipPrevoza) : ?>
                                                <?php foreach ($tipPrevoza  as $prevoz) : ?>
                                                <i class="main-color-text mr-2 icon-<?php echo $prevoz['value']; ?>"
                                                    data-toggle="tooltip" data-placement="right"
                                                    title="<?php echo $prevoz['label']; ?>"></i>
                                                <?php endforeach; ?>
                                                <?php endif; ?>
                                            </span>
                                            <?php endif; ?>
                                            <?php the_sub_field('cena'); ?>
                                    </div>
                                    <div class="bottom d-flex justify-content-between">
                                        <span class="small-text d-block"><?php the_sub_field('termini'); ?></span>
                                        <span class="small-text"><?php the_sub_field('nocenja'); ?></span>
                                    </div>
                                </a>
                                <?php endwhile; ?>
                                <div class="single-item">
                                    <a href="<?php the_field('btn_link') ?>"
                                        class="second-btn w-100 mb-2"><?php the_field('btn_text') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>
            <!-- Tabs & banners (END) -->
        </div>
        <div class="middle-background" style="background-image: url('<?php the_field('middle_bg') ?>');">
            <!-- Popular destination  (START) -->
            <div class="container popular-destination">
                <div class="row">
                    <div
                        class="col-xl-3 col-lg-4 col-md-5 col-sm-12 d-flex flex-column justify-content-center align-items-start">
                        <h2 class="popular-title"><?php the_field('ime_destinacije'); ?></h2>
                        <h3 class="popular-subtitle"><?php the_field('podnaslov_destinacije'); ?></h3>
                        <a href="<?php the_field('link_dest'); ?>"
                            class="second-btn second-color btn-sm"><?php the_field('dugme_dest'); ?></a>
                    </div>
                    <div class="col-xl-9 col-lg-8 col-md-7 col-sm-12">
                        <?php if (have_rows('popularne_destinacije')) : ?>
                        <div class="owl-carousel popular-destination-slider owl-theme mb-5">
                            <?php while (have_rows('popularne_destinacije')) : the_row(); ?>
                            <?php $link = get_sub_field('link'); ?>
                            <a href="<?php echo get_permalink($link); ?>">
                                <div class="slider-item temp-bg">
                                    <?php echo get_the_post_thumbnail($link, 'ponude-img', array('class' => 'object-fit-center')); ?>
                                    <div class="blue-bg"></div>
                                    <div class="title position-absolute">
                                        <h2><?php the_sub_field('naslov'); ?></h2>
                                        <h3><?php the_sub_field('podnaslov'); ?></h3>
                                    </div>
                                </div>
                            </a>
                            <?php endwhile; ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <!-- Popular destination (END) -->
            <!-- Destination pack (START) -->
            <?php if (have_rows('destinacije_baner')) : ?>
            <div class="container destination-pack">
                <div class="row">
                    <?php $i = 1; ?>
                    <?php while (have_rows('destinacije_baner')) : the_row(); ?>
                    <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mobile-margin-top">
                        <div class="destination-box db-<?php if ($i == 2) : ?>2<?php endif; ?>">
                            <?php $slika = get_sub_field('slika'); ?>
                            <?php echo wp_get_attachment_image($slika, 'full', "", ["class" => "object-fit-center mobile-hide"]); ?>
                            <a class="w-100 h-100" href="<?php the_sub_field('link') ?>">
                                <div class="destination-title">
                                    <h2 class="destination-title"><?php the_sub_field('naslov'); ?></h2>
                                </div>
                            </a>
                            <?php if (have_rows('mali_baneri')) : ?>
                            <div class="destination-items d-flex justify-content-between">
                                <?php while (have_rows('mali_baneri')) : the_row(); ?>
                                <a href="<?php the_sub_field('link') ?>"
                                    class="item position-relative border-radius-mobile">
                                    <?php $slika = get_sub_field('slika'); ?>
                                    <?php echo wp_get_attachment_image($slika, 'full', "", ["class" => "object-fit-center border-radius-mobile"]); ?>
                                    <div class="blue-bg border-radius-mobile"></div>
                                    <h5 class="mb-0 position-absolute m-3"><?php the_sub_field('naslov'); ?></h5>
                                </a>
                                <?php endwhile; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php $i++; ?>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <!-- Destination pack (END) -->
        </div>
        <div class="bottom-background" style="background-image: url('<?php the_field('bottom_bg') ?>');">
            <!-- Homepage tables (START) -->
            <div class="container homepage-tables">
                <div class="row">
                    <?php if (have_rows('leva_tabela', 'option')) : ?>
                    <?php while (have_rows('leva_tabela', 'option')) : the_row(); ?>
                    <?php $naslovTabele = get_sub_field('naslov_tabele', 'option');
							$brojac = 0; //Dodajemo broj na svaki modal kako bi se razlikovali zbog smena 8d/7n, 11d/10n itd.
							$naslovTabeleStrip = preg_replace('/\s+/', '', $naslovTabele);
							$brojModala = 1;
							?>
                    <?php if (have_rows('smene', 'option')) : ?>
                    <?php while (have_rows('smene', 'option')) : the_row(); ?>
                    <?php $smena = get_sub_field('smena', 'option');
									$tabelaModalID = clean($naslovTabeleStrip . $brojac);
									if ($brojModala == 1) {
										$brMod = 1;
									} else if ($brojModala == 2) {
										$brMod = 13;
									} else if ($brojModala == 3) {
										$brMod = 26;
									}
									?>
                    <!-- Table section modal (START) -->
                    <div class="modal fade bd-example-modal-lg tableModalHomepage" id="<?php echo $tabelaModalID ?>"
                        tabindex="-1" role="dialog" aria-labelledby="<?php echo $tabelaModalID ?>Title"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title d-flex" id="<?php echo $tabelaModalID ?>Title">
                                        <h5 class="font-weight-bold main-color-text"><?php echo $naslovTabele ?> </h5>
                                        &nbsp;-&nbsp;<td><?php echo $smena ?></td>
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <ul class="nav nav-tabs" id="myTab<?php echo $brMod ?>" role="tablist">
                                        <!-- Stampanje repeater-a -->
                                        <?php $i = $brMod; ?>
                                        <?php if (have_rows('polasci', 'option')) : ?>
                                        <?php while (have_rows('polasci', 'option')) : the_row();
																$mesec = get_sub_field('mesec', 'option') . $i;
															?>
                                        <!-- Sadrzaj -->
                                        <li class="nav-item">
                                            <a class="nav-link <?php if ($i == 1 || $i == 13 || $i == 26) : ?> active<?php endif; ?>"
                                                id="<?php echo $mesec ?>-tab" data-toggle="tab"
                                                href="#<?php echo $mesec ?>" role="tab"
                                                aria-controls="<?php echo $mesec ?>"
                                                aria-selected="true"><?php the_sub_field('mesec', 'option'); ?></a>
                                        </li>
                                        <?php $i++;
															endwhile; ?>
                                        <?php endif; ?>
                                    </ul>
                                    <div class="tab-content" id="myTabContent<?php echo $brMod ?>">
                                        <!-- Stampanje repeater-a -->
                                        <?php $i = $brMod; ?>
                                        <?php if (have_rows('polasci', 'option')) : ?>
                                        <?php while (have_rows('polasci', 'option')) : the_row();
																$mesec = get_sub_field('mesec', 'option') . $i;
															?>
                                        <div class="tab-pane fade show <?php if ($i == 1 || $i == 13 || $i == 26) : ?> active<?php endif; ?>"
                                            id="<?php echo $mesec ?>" role="tabpanel"
                                            aria-labelledby="<?php echo $mesec ?>-tab">
                                            <div class="polasci-tab">
                                                <?php $sviPolasci = get_sub_field('polasci', 'option'); ?>
                                                <?php foreach (preg_split("/((\r?\n)|(\r\n?))/", $sviPolasci) as $line) : ?>
                                                <span class="departure-date"><?php echo $line; ?></span>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                        <?php $i++;
															endwhile; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Table section modal (END) -->
                    <?php $brojModala++;
									$brojac++;
								endwhile; ?>
                    <?php endif; ?>
                    <div class="col-lg-6 col-sm-12 ">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <div class="top-title d-flex align-items-center">
                                <div class="flag"
                                    style="background-image: url('<?php the_sub_field('zastava', 'option'); ?>');">
                                </div>
                                <h3 class="mb-0 ml-2"><?php the_sub_field('naslov_tabele', 'option'); ?></h3>
                            </div>
                            <a href="<?php the_sub_field('link_do_stranice'); ?>"
                                class="second-btn small-text mobile-hide"><?php the_sub_field('vidi_vise_tekst', 'option'); ?>
                                <i class="icon-arrow-right-2 ml-2"></i></a>
                        </div>
                        <!-- LEVA TABELA (START) -->
                        <table class="red-table mobile-hide">
                            <thead>
                                <tr>
                                    <td>Hotel</td>
                                    <td>Usluga</td>
                                    <?php if (have_rows('smene', 'option')) : ?>
                                    <?php while (have_rows('smene', 'option')) : the_row(); ?>
                                    <td><?php the_sub_field('smena', 'option'); ?></td>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Stampanje repeater-a -->
                                <?php if (have_rows('hoteli', 'option')) : ?>
                                <?php while (have_rows('hoteli', 'option')) : the_row(); ?>
                                <!-- Sadrzaj -->
                                <?php the_sub_field('example'); ?>
                                <?php
												$hotel = get_sub_field('hotel', 'option');
												$hotelID = esc_html($hotel->ID);
												if ($hotel) : ?>
                                <tr>
                                    <td class="d-flex flex-column">
                                        <span class="main-info"><?php echo esc_html($hotel->post_title); ?></span>
                                        <span class="medium-text">Hurgada</span>
                                    </td>
                                    <td class="medium-text">
                                        <?php
															$uslugaHotela = get_field('usluga_hotela', $hotelID);
															if ($uslugaHotela) : ?>
                                        <?php foreach ($uslugaHotela  as $usluga) : ?>
                                        <span data-toggle="tooltip" data-placement="top"
                                            title="<?php echo $usluga['label']; ?>"><?php echo $usluga['label']; ?></span>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </td>
                                    <!-- Stampanje repeater-a -->
                                    <?php $brojac = 0; ?>
                                    <?php if (have_rows('tabela_sa_cenama', $hotelID)) : ?>
                                    <?php while (have_rows('tabela_sa_cenama', $hotelID)) : the_row(); ?>
                                    <?php $tabelaModalID = clean($naslovTabeleStrip . $brojac); ?>
                                    <!-- Sadrzaj -->
                                    <td class="main-info" data-toggle="modal"
                                        data-target="#<?php echo $tabelaModalID ?>">
                                        <?php the_sub_field('Cena'); ?>
                                        <div class="small-text d-flex">Termini <i class="icon-info ml-1"
                                                data-toggle="tooltip" data-placement="top"
                                                title="Klikni ovde da bi video sve polaske">i</i> </div>
                                    </td>
                                    <?php $brojac++;
															endwhile; ?>
                                    <?php endif; ?>
                                </tr>
                                <?php endif; ?>
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- LEVA TABELA (END) -->
                        <span class="warning small-text mobile-hide">
                            * Pređite pokazivačem miša preko "i" ikonice da biste videli termine
                        </span>
                        <!-- LEVA TABELA MOBILE (START) -->
                        <div class="d-flex justify-content-between mobile-show">
                            <span class="font-weight-bold">Hoteli</span>
                            <div class="d-flex">
                                <?php if (have_rows('smene', 'option')) : ?>
                                <?php while (have_rows('smene', 'option')) : the_row(); ?>
                                <span
                                    class="ml-2 font-weight-bold mobile-price"><?php the_sub_field('smena', 'option'); ?></span>
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div id="red_table" class="red-table">
                            <?php if (have_rows('hoteli', 'option')) : ?>
                            <?php while (have_rows('hoteli', 'option')) : the_row();
											$hotel = get_sub_field('hotel', 'option');
											$hotelID = esc_html($hotel->ID); ?>
                            <div class="item">
                                <div class="card-header" id="heading<?php echo $hotelID ?>">
                                    <h5 class="mb-0">
                                        <a class="d-flex justify-content-between" data-toggle="collapse"
                                            data-target="#collapse<?php echo $hotelID ?>" aria-expanded="true"
                                            aria-controls="collapse<?php echo $hotelID ?>">
                                            <div class="d-flex flex-column">
                                                <span
                                                    class="main-info"><?php echo esc_html($hotel->post_title); ?></span>
                                                <span class="medium-text">Hurgada</span>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <?php $brojac = 0; ?>
                                                <?php if (have_rows('tabela_sa_cenama', $hotelID)) : ?>
                                                <?php while (have_rows('tabela_sa_cenama', $hotelID)) : the_row(); ?>
                                                <?php $tabelaModalID = clean($naslovTabeleStrip . $brojac); ?>
                                                <div class="d-flex flex-column ml-3" data-toggle="modal"
                                                    data-target="#<?php echo $tabelaModalID ?>">
                                                    <span class="main-color-text font-weight-medium">od
                                                        <?php the_sub_field('Cena'); ?></span>
                                                    <div class="small-text d-flex">Termini <i class="icon-info ml-1"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="Klikni ovde da bi video sve polaske">i</i> </div>
                                                </div>
                                                <?php $brojac++; endwhile; ?>
                                                <?php endif; ?>
                                            </div>
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse<?php echo $hotelID ?>" class="collapse "
                                    aria-labelledby="heading<?php echo $hotelID ?>" data-parent="#red_table">
                                    <div class="card-body">
                                        <table class="mobile-data-table">
                                            <thead>
                                                <tr>
                                                    <td>Usluga</td>
                                                    <!-- <?php if (have_rows('smene', 'option')) : ?>
																		<?php while (have_rows('smene', 'option')) : the_row(); ?>
																			<td><?php the_sub_field('smena', 'option'); ?></td>
																		<?php endwhile; ?>
																	<?php endif; ?> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if ($hotel) : ?>
                                                <tr>
                                                    <td class="medium-text">
                                                        <?php
																			$uslugaHotela = get_field('usluga_hotela', $hotelID);
																			if ($uslugaHotela) : ?>
                                                        <?php foreach ($uslugaHotela  as $usluga) : ?>
                                                        <span data-toggle="tooltip" data-placement="top"
                                                            title="<?php echo $usluga['label']; ?>"><?php echo $usluga['label']; ?></span>
                                                        <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <!-- Stampanje repeater-a -->
                                                    <!-- <?php $brojac = 0; ?>
																		<?php if (have_rows('tabela_sa_cenama', $hotelID)) : ?>
																			<?php while (have_rows('tabela_sa_cenama', $hotelID)) : the_row(); ?>
																				<?php $tabelaModalID = clean($naslovTabeleStrip . $brojac); ?>
																				<td class="main-info">
																					<?php the_sub_field('Cena'); ?>
																					<div class="small-text d-flex" data-toggle="modal" data-target="#<?php echo $tabelaModalID ?>">Termini <i class="icon-info ml-1" data-toggle="tooltip" data-placement="top" title="Klikni ovde da bi video sve polaske">i</i> </div>
																				</td>
																			<?php $brojac++;
																			endwhile; ?>
																		<?php endif; ?> -->
                                                </tr>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                        <a href="<?php echo get_permalink($hotelID); ?>" class="view-hotel">Pogledaj
                                            hotel</a>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                        <!-- LEVA TABELA MOBILE (END) -->
                        <a href="" class="primary-btn primary-color w-100 mb-5 mobile-show">Pogledajte celokupnu ponudu
                            <i class="icon-arrow-right-2"></i></a>
                    </div>
                    <?php endwhile; ?>
                    <?php endif;
					wp_reset_postdata(); ?>
                    <?php if (have_rows('desna_tabela', 'option')) : ?>
                    <?php while (have_rows('desna_tabela', 'option')) : the_row(); ?>
                    <?php $naslovTabele = get_sub_field('naslov_tabele', 'option');
							$brojac = 0; //Dodajemo broj na svaki modal kako bi se razlikovali zbog smena 8d/7n, 11d/10n itd.
							$naslovTabeleStrip = preg_replace('/\s+/', '', $naslovTabele);
							$brojModala = 1;
							?>
                    <?php if (have_rows('smene_dubai', 'option')) : ?>
                    <?php while (have_rows('smene_dubai', 'option')) : the_row(); ?>
                    <?php $smena = get_sub_field('smena', 'option');
									$tabelaModalID = clean($naslovTabeleStrip . $brojac);
									if ($brojModala == 1) {
										$brMod = 27;
									} else if ($brojModala == 2) {
										$brMod = 40;
									} else if ($brojModala == 3) {
										$brMod = 53;
									}
									?>
                    <!-- Table section modal (START) -->
                    <div class="modal fade bd-example-modal-lg tableModalHomepage" id="<?php echo $tabelaModalID ?>"
                        tabindex="-1" role="dialog" aria-labelledby="<?php echo $tabelaModalID ?>Title"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title d-flex" id="<?php echo $tabelaModalID ?>Title">
                                        <h5 class="font-weight-bold main-color-text"><?php echo $naslovTabele ?> </h5>
                                        &nbsp;-&nbsp;<td><?php echo $smena ?></td>
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <ul class="nav nav-tabs" id="myTab<?php echo $brMod ?>" role="tablist">
                                        <!-- Stampanje repeater-a -->
                                        <?php $i = $brMod; ?>
                                        <?php if (have_rows('polasci', 'option')) : ?>
                                        <?php while (have_rows('polasci', 'option')) : the_row();
																$mesec = get_sub_field('mesec', 'option') . $i;
															?>
                                        <!-- Sadrzaj -->
                                        <li class="nav-item">
                                            <a class="nav-link <?php if ($i == 27 || $i == 40 || $i == 53) : ?> active<?php endif; ?>"
                                                id="<?php echo $mesec ?>-tab" data-toggle="tab"
                                                href="#<?php echo $mesec ?>" role="tab"
                                                aria-controls="<?php echo $mesec ?>"
                                                aria-selected="true"><?php the_sub_field('mesec', 'option'); ?></a>
                                        </li>
                                        <?php $i++;
															endwhile; ?>
                                        <?php endif; ?>
                                    </ul>
                                    <div class="tab-content" id="myTabContent<?php echo $brMod ?>">
                                        <!-- Stampanje repeater-a -->
                                        <?php $i = $brMod; ?>
                                        <?php if (have_rows('polasci', 'option')) : ?>
                                        <?php while (have_rows('polasci', 'option')) : the_row();
																$mesec = get_sub_field('mesec', 'option') . $i;
															?>
                                        <div class="tab-pane fade show <?php if ($i == 27 || $i == 40 || $i == 53) : ?> active<?php endif; ?>"
                                            id="<?php echo $mesec ?>" role="tabpanel"
                                            aria-labelledby="<?php echo $mesec ?>-tab">
                                            <div class="polasci-tab">
                                                <?php $sviPolasci = get_sub_field('polasci', 'option'); ?>
                                                <?php foreach (preg_split("/((\r?\n)|(\r\n?))/", $sviPolasci) as $line) : ?>
                                                <span class="departure-date"><?php echo $line; ?></span>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                        <?php $i++;
															endwhile; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Table section modal (END) -->
                    <?php $brojModala++;
									$brojac++;
								endwhile; ?>
                    <?php endif; ?>
                    <div class="col-lg-6 col-sm-12 ">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <div class="top-title d-flex align-items-center">
                                <div class="flag"
                                    style="background-image: url('<?php the_sub_field('zastava', 'option'); ?>');">
                                </div>
                                <h3 class="mb-0 ml-2"><?php the_sub_field('naslov_tabele', 'option'); ?></h3>
                            </div>
                            <a href="<?php the_sub_field('link_do_stranice'); ?>"
                                class="second-btn small-text mobile-hide"><?php the_sub_field('vidi_vise_tekst', 'option'); ?>
                                <i class="icon-arrow-right-2  ml-2"></i></a>
                        </div>
                        <!-- DESNA TABELA (START) -->
                        <table class="green-table mobile-hide">
                            <thead>
                                <tr>
                                    <td>Hotel</td>
                                    <td>Usluga</td>
                                    <?php if (have_rows('smene_dubai', 'option')) : ?>
                                    <?php while (have_rows('smene_dubai', 'option')) : the_row(); ?>
                                    <td><?php the_sub_field('smena', 'option'); ?></td>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Stampanje repeater-a -->
                                <?php if (have_rows('hoteli_dubai', 'option')) : ?>
                                <?php while (have_rows('hoteli_dubai', 'option')) : the_row(); ?>
                                <!-- Sadrzaj -->
                                <?php
												$hotel = get_sub_field('hotel', 'option');
												$hotelID = esc_html($hotel->ID);
												if ($hotel) : ?>
                                <tr>
                                    <td class="d-flex flex-column">
                                        <span class="main-info"><?php echo esc_html($hotel->post_title); ?></span>
                                        <span class="medium-text">Hurgada</span>
                                    </td>
                                    <td class="medium-text">
                                        <?php
															$uslugaHotela = get_field('usluga_hotela', $hotelID);
															if ($uslugaHotela) : ?>
                                        <?php foreach ($uslugaHotela  as $usluga) : ?>
                                        <span data-toggle="tooltip" data-placement="top"
                                            title="<?php echo $usluga['label']; ?>"><?php echo $usluga['label']; ?></span>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </td>
                                    <!-- Stampanje repeater-a -->
                                    <?php $brojac = 0; ?>
                                    <?php if (have_rows('tabela_sa_cenama', $hotelID)) : ?>
                                    <?php while (have_rows('tabela_sa_cenama', $hotelID)) : the_row(); ?>
                                    <?php $tabelaModalID = clean($naslovTabeleStrip . $brojac); ?>
                                    <!-- Sadrzaj -->
                                    <td class="main-info" data-toggle="modal"
                                        data-target="#<?php echo $tabelaModalID ?>">
                                        <?php the_sub_field('Cena'); ?>
                                        <div class="small-text d-flex">Termini <i class="icon-info ml-1"
                                                data-toggle="tooltip" data-placement="top"
                                                title="Klikni ovde da bi video sve polaske">i</i> </div>
                                    </td>
                                    <?php $brojac++;
															endwhile; ?>
                                    <?php endif; ?>
                                </tr>
                                <?php endif; ?>
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- DESNA TABELA (END) -->
                        <span class="warning small-text mobile-hide">
                            * Pređite pokazivačem miša preko "i" ikonice da biste videli termine
                        </span>
                        <!-- DESNA TABELA MOBILE (START) -->
                        <div class="d-flex justify-content-between mobile-show">
                            <span class="font-weight-bold">Hoteli</span>
                            <div class="d-flex">
                                <?php if (have_rows('smene_dubai', 'option')) : ?>
                                <?php while (have_rows('smene_dubai', 'option')) : the_row(); ?>
                                <span
                                    class="ml-2 font-weight-bold mobile-price"><?php the_sub_field('smena', 'option'); ?></span>
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div id="red_table" class="red-table">
                            <?php if (have_rows('hoteli_dubai', 'option')) : ?>
                            <?php while (have_rows('hoteli_dubai', 'option')) : the_row();
											$hotel = get_sub_field('hotel', 'option');
											$hotelID = esc_html($hotel->ID); ?>
                            <div class="item">
                                <div class="card-header" id="heading<?php echo $hotelID ?>">
                                    <h5 class="mb-0">
                                        <a class="d-flex justify-content-between" data-toggle="collapse"
                                            data-target="#collapse<?php echo $hotelID ?>" aria-expanded="true"
                                            aria-controls="collapse<?php echo $hotelID ?>">
                                            <div class="d-flex flex-column">
                                                <span
                                                    class="main-info"><?php echo esc_html($hotel->post_title); ?></span>
                                                <span class="medium-text">Hurgada</span>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <?php $brojac = 0; ?>
                                                <?php if (have_rows('tabela_sa_cenama', $hotelID)) : ?>
                                                <?php while (have_rows('tabela_sa_cenama', $hotelID)) : the_row(); ?>
                                                <?php $tabelaModalID = clean($naslovTabeleStrip . $brojac); ?>
                                                <div class="d-flex flex-column ml-3" data-toggle="modal"
                                                    data-target="#<?php echo $tabelaModalID ?>">
                                                    <span class="main-color-text font-weight-medium">od
                                                        <?php the_sub_field('Cena'); ?></span>
                                                    <div class="small-text d-flex">Termini <i class="icon-info ml-1"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="Klikni ovde da bi video sve polaske">i</i> </div>
                                                </div>
                                                <?php $brojac++; endwhile; ?>
                                                <?php endif; ?>
                                            </div>
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse<?php echo $hotelID ?>" class="collapse "
                                    aria-labelledby="heading<?php echo $hotelID ?>" data-parent="#red_table">
                                    <div class="card-body">
                                        <table class="mobile-data-table">
                                            <thead>
                                                <tr>
                                                    <td>Usluga</td>
                                                    <!-- <?php if (have_rows('smene', 'option')) : ?>
																		<?php while (have_rows('smene', 'option')) : the_row(); ?>
																			<td><?php the_sub_field('smena', 'option'); ?></td>
																		<?php endwhile; ?>
																	<?php endif; ?> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if ($hotel) : ?>
                                                <tr>
                                                    <td class="medium-text">
                                                        <?php
																			$uslugaHotela = get_field('usluga_hotela', $hotelID);
																			if ($uslugaHotela) : ?>
                                                        <?php foreach ($uslugaHotela  as $usluga) : ?>
                                                        <span data-toggle="tooltip" data-placement="top"
                                                            title="<?php echo $usluga['label']; ?>"><?php echo $usluga['label']; ?></span>
                                                        <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <!-- Stampanje repeater-a -->
                                                    <!-- <?php $brojac = 0; ?>
																		<?php if (have_rows('tabela_sa_cenama', $hotelID)) : ?>
																			<?php while (have_rows('tabela_sa_cenama', $hotelID)) : the_row(); ?>
																				<?php $tabelaModalID = clean($naslovTabeleStrip . $brojac); ?>
																				<td class="main-info">
																					<?php the_sub_field('Cena'); ?>
																					<div class="small-text d-flex" data-toggle="modal" data-target="#<?php echo $tabelaModalID ?>">Termini <i class="icon-info ml-1" data-toggle="tooltip" data-placement="top" title="Klikni ovde da bi video sve polaske">i</i> </div>
																				</td>
																			<?php $brojac++;
																			endwhile; ?>
																		<?php endif; ?> -->
                                                </tr>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                        <a href="<?php echo get_permalink($hotelID); ?>" class="view-hotel">Pogledaj
                                            hotel</a>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                        <!-- DESNA TABELA MOBILE (END) -->
                        <a href="" class="primary-btn primary-color w-100 mb-5 mobile-show">Pogledajte celokupnu ponudu
                            <i class="icon-arrow-right-2"></i></a>
                    </div>
                    <?php endwhile; ?>
                    <?php endif;
					wp_reset_postdata(); ?>
                </div>
            </div>
            <!-- Homepage tables (END) -->
            <!-- Note (START) -->
            <div class="container note">
                <div class="row">
                    <div class="col-12">
                        <div class="note-box">
                            <span class="note-title"><i class="icon-information mr-1"></i> Napomena</span>
                            <p><?php the_field('napomena_ponude'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Note (END) -->
        </div>
        <div class="container">
            <?php
			// Start the Loop.
			// while ( have_posts() ) :
			// 	the_post();
			// 	get_template_part( 'template-parts/content/content', 'page' );
			// endwhile; // End the loop.
			?>
        </div>
    </main><!-- #main -->
</div><!-- #primary -->





<div>
    <div id="filteri-home" class="secondary-section filteri-home samo-pocetna filter-telefon">
        <div class='container'>
            <!-- <script src="jquery.min.js"></script> -->
            <!-- <div id="apartmaniFilter">
                    <?php # require('function/filter-apartmani.php'); ?>
                    <div class="ucitavam"></div>
            </div> -->
        </div>
    </div>
</div>

<?php
get_footer();