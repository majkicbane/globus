<?php
/**
 * Template Name: 4. dubina - Objekat
 *
 * @package TA Pluton
 */
get_header(); ?>
<!-- Posalji upit kartica -->
<button class="inquiry-btn"><i class="icon-email"></i></button>
<a class="viber-btn" href="viber://chat/?number=%2B381604150513">   
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/viber-sticker.svg" alt="">
</a>
<div class="inquiry-card">
    <h5 class="title">POŠALJITE UPIT</h5>
    <div class="inquiry-form">
        <?php $form =  get_field('forma_za_upit', 'options') ?>
        <?php echo do_shortcode($form); ?>
    </div>
</div>
<?php $sezona =  get_field('izaberi_sezonu'); ?>
<div class="header-image" style="background-image: url('<?php the_field($sezona,'option'); ?>')">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-between align-items-center top-head-title">
                <div class="d-flex align-items-start hotel-title">
                    <h2 class="mb-0 d-flex flex-column">
                        <?php the_title(); ?>
                        <?php if(get_field('tip_smestaja'== 'Hotel')): ?>
                        <div class="star-rating">
                            <?php
							$numberOfStarsField = get_field('broj_zvezdica');
							$numberOfStars = $numberOfStarsField - 0;
							if(gettype($numberOfStars) == 'double'){
								$roundedDown = floor($numberOfStars);
								$roundedUp = ceil($numberOfStars);
								for($i=0;$i<$roundedDown;$i++){
									echo "<i class='icon-full-star'></i>";
								}
								echo "<i class='icon-half-star'></i>";
								for($j=0;$j<(5-$roundedUp);$j++){
									echo "<i class='icon-star'></i>";
								}
							}else{
								for($i=0;$i<$numberOfStars;$i++){
									echo "<i class='icon-full-star'></i>";
								}
								for($j=0;$j<(5-$numberOfStars);$j++){
									echo "<i class='icon-star'></i>";
								}
							}
							if($numberOfStars == '1'){
								for($i=0;$i<$roundedDown;$i++){
									echo "<i class='icon-full-star'></i>";
								}
							}
							?>
                        </div>
                        <?php endif; ?>
                    </h2>
                    <div class="d-flex align-items-center">
                        <i class="icon-pin mt-2"></i>
                        <span class="mt-2">
                            <?php
							$grandparent_get = get_post($parent);
							$grandparent = $grandparent_get->post_parent;
							if( $ancs = get_ancestors($post->ID,'page') ) {
								for($i=0;$i<2;$i++){
									echo  get_page( $ancs[$i] )->post_title;
									echo ($i== 0) ? ', ' : '';
								}
							} 
							?>
                        </span>
                    </div>
                </div>
                <div class="d-flex align-items-center hotel-share">
                    <span>Podeli: </span>
                    <?php require('function/share-horizontal.php'); ?>
                </div>
            </div>
            <div class="col-xl-7 col-lg-6 col-sm-12">
                <?php 
				$slika = get_field('galerija');
				$size = 'full'; // (thumbnail, medium, large, full or custom size)
				?>
                <?php if(get_field('galerija')): ?>
                <div class="position-relative">
                    <div id="sync1" class="slider owl-carousel mb-3">
                        <?php if( $slika ): ?>
                        <?php foreach( $slika as $image_id ): ?>
                        <div class="item">
                            <?php echo wp_get_attachment_image($image_id, $size, ["class" => "object-fit-center border-radius-mobile"] ); ?>
                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <span class="image-info"><i class="icon-information mr-2"></i>Slike su informativnog
                        karaktera</span>
                </div>
                <div id="sync2" class="navigation-thumbs owl-carousel">
                    <?php if( $slika ): ?>
                    <?php foreach( $slika as $image_id ): ?>
                    <div class="item">
                        <?php echo wp_get_attachment_image($image_id, $size, ["class" => "object-fit-center border-radius-mobile"] ); ?>
                    </div>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <?php else : ?>
                <div style="background-image:url('<?php echo get_stylesheet_directory_uri(); ?>/img/noimg.png')"
                    class="no-img"></div>
                <?php endif; ?>
            </div>
            <div class="col-xl-5 col-lg-6 col-sm-12">
                <div class="info-card">
                    <h3>Osnovne informacije</h3>
                    <div class="info-icons">
                        <?php
					$icons = get_field('usluge');
					if( $icons ): ?>
                        <?php foreach( $icons  as $icon ): ?>
                        <div class="icon">
                            <i class="icon-<?php echo $icon['value']; ?>"></i>
                            <span><?php echo $icon['label']; ?></span>
                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>
                        <!-- <?php
					$colors = get_field('usluge');
					if( $colors ): ?>
						<?php foreach( $colors as $color ): ?>
							<div class="icon">
							<i class="icon-<?php echo $color; ?>"></i>
							<span><?php echo $color; ?></span>
							</div>
						<?php endforeach; ?>
					<?php endif; ?> -->
                    </div>
                    <?php if(get_field('sadrzaj_vile')): ?>
                    <div class="d-flex mb-4 mt-4 more-info">
                        <span class="subtitle">
                            Sadržaj vile:
                        </span>
                        <div class="d-flex">
                            <span><?php the_field('sadrzaj_vile') ?></span>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if(get_field('sadrzaj_studia')): ?>
                    <div class="d-flex more-info">
                        <span class="subtitle">
                            Sadržaj studia:
                        </span>
                        <div class="d-flex">
                            <span><?php the_field('sadrzaj_studia') ?></span>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-xl-9 col-lg-8 col-sm-12">
            <div class="hotel-tabs">

                <div class="hotel-tabs">
                    <ul class="nav nav-tabs">
                        <li class="desktop-tab">
                            <a class="active" data-toggle="tab" href="#infoTAB"><i class="icon-information"></i>
                                INFORMACIJE</a>
                        </li>
                        <?php if(get_field('program_putovanja')): ?>
                        <li class="desktop-tab">
                            <a id="program-tab" data-toggle="tab" href="#programTAB"><i class="icon-journey"></i>
                                PROGRAM
                                PUTOVANJA</a>
                        </li>
                        <?php endif; ?>
                        <?php if(get_field('cenovnik')): ?>
                        <li class="desktop-tab">
                            <a id="price-tab" data-toggle="tab" href="#priceTAB"><i class="icon-wallet"></i>
                                CENOVNIK</a>
                        </li>
                        <?php endif; ?>
                        <?php if(get_field('mapa_vile')): ?>
                        <li class="desktop-tab">
                            <a id="map-tab" data-toggle="tab" href="#mapTAB"><i class="icon-place"></i> MAPA</a>
                        </li>
                        <?php endif; ?>
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane active" id="infoTAB">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="expanded-tab" data-toggle="collapse" data-parent=".tab-pane"
                                            href="#collapseinfo">
                                            <i class="icon-information"></i> INFORMACIJE
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseinfo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="d-flex justify-content-between align-items-center info-tab-top">
                                            <?php
						$tipPrevoza = get_field('tip_prevoza');
						if( $tipPrevoza ): ?>
                                            <div class="tip-prevoza">
                                                <?php foreach( $tipPrevoza  as $prevoz ): ?>
                                                <div class="icon">
                                                    <i class="icon-<?php echo $prevoz['value']; ?>"></i>
                                                    <span><?php echo $prevoz['label']; ?></span>
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <?php endif; ?>
                                            <?php
						$uslugaHotela = get_field('usluga_hotela');
						if( $uslugaHotela ): ?>
                                            <div class="usluga-hotela">
                                                <?php foreach( $uslugaHotela  as $usluga ): ?>
                                                <div class="icon">
                                                    <span data-toggle="tooltip" data-placement="top"
                                                        title="Tooltip on top"><?php echo $usluga['label']; ?></span>
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                        <?php the_content();?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="programTAB">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="expanded-tab collapsed" data-toggle="collapse" data-parent=".tab-pane"
                                            href="#collapseprogramTAB">
                                            <i class="icon-information"></i> PROGRAM PUTOVANJA
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseprogramTAB" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <?php the_field('program_putovanja') ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="priceTAB">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="expanded-tab collapsed" data-toggle="collapse" data-parent=".tab-pane"
                                            href="#collapsepriceTAB">
                                            <i class="icon-information"></i> CENOVNIK
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsepriceTAB" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <?php the_field('cenovnik') ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="mapTAB">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="expanded-tab collapsed" data-toggle="collapse" data-parent=".tab-pane"
                                            href="#collapsemapTAB">
                                            <i class="icon-information"></i> MAPA
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsemapTAB" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <?php the_field('mapa_vile') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="tab-pane">
                    <div id="includesOrNot">
                        <?php if(get_field('obuhvata')): ?>
                        <div class="item">
                            <div class="card-header" id="headingObuhvata" data-toggle="collapse"
                                data-target="#collapseObuhvata" aria-expanded="true" aria-controls="collapseObuhvata">
                                <h5 class="mb-0 d-flex align-items-center justify-content-between">
                                    <button class="btn btn-link">
                                        AranŽman obuhvata:
                                    </button>
                                    <i class="icon-arrow-down"></i>
                                </h5>
                            </div>
                            <div id="collapseObuhvata" class="collapse " aria-labelledby="headingObuhvata"
                                data-parent="#includesOrNot">
                                <div class="card-body">
                                    <?php the_field('obuhvata') ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(get_field('neobuhvata')): ?>
                        <div class="item">
                            <div class="card-header" id="headingNeObuhvata" data-toggle="collapse"
                                data-target="#collapseNeObuhvata" aria-expanded="false"
                                aria-controls="collapseNeObuhvata">
                                <h5 class="mb-0 d-flex align-items-center justify-content-between">
                                    <button class="btn btn-link collapsed">
                                        AranŽman NE obuhvata:
                                    </button>
                                    <i class="icon-arrow-down"></i>
                                </h5>
                            </div>
                            <div id="collapseNeObuhvata" class="collapse" aria-labelledby="headingNeObuhvata"
                                data-parent="#includesOrNot">
                                <div class="card-body">
                                    <?php the_field('neobuhvata') ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <!-- Napomena -->
                    <?php if( get_field('tekst_napomene','option') ): ?>
                    <div id="warningAccordion">
                        <div class="item">
                            <div class="card-header" id="headingThree" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                <h5 class="mb-0 d-flex align-items-center justify-content-between">
                                    <button class="btn btn-link">
                                        Napomene:
                                    </button>
                                    <i class="icon-arrow-down"></i>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse " aria-labelledby="headingThree"
                                data-parent="#warningAccordion">
                                <div class="card-body">
                                    <?php $nazivNapomene = get_field('napomena_objekat'); ?>
                                    
                                    <!-- Štampanje repeater-a -->
                                    <?php if (have_rows('napomene_global','option')) : ?>	
                                        <?php while (have_rows('napomene_global','option')) : the_row(); ?>
                                            <!-- Sadržaj repeater-a -->
                                            <?php 
                                                if(get_sub_field('naziv','option')==$nazivNapomene){
                                                    echo get_sub_field('tekst_napomene','option');
                                                }
                                            ?>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-sm-12">
            <aside class="side-card mb-4">
                <div class="title">
                    <h5><?php echo get_post( $ancs[1] )->post_title; ?> - PONUDA</h5>
                </div>
                <div class="items">
                    <?php
						global $post;
						$currentID = $post->ID;
						$parentPage = $post->post_parent;
						// Drzava
						$state = $ancs[1];
						$pages_no = get_pages("child_of=$state&depth=1&parent=$state");
						// Broj destinacija u drzavi
						$count = count($pages_no);
						$children = wp_list_pages("title_li=&child_of=" . $state . "&echo=0");
						$get_children_array = get_children($state);
						foreach($get_children_array as $child){
							$childID = $child->ID;
							$pageName = $child->post_title;
							// var_dump($temp);
							$pages_no2 = get_pages("child_of=$childID&depth=1&parent=$childID");
							$brojVilaDestinacija = count($pages_no2);
						// echo $get_children_array;
					?>
                    <?php if($brojVilaDestinacija != 'empty'): ?>
                    <a href="" class="d-flex single-item justify-content-between align-items-center">
                        <h6 class="text-uppercase m-0"><?php echo $pageName ?></h6>
                        <div class="badge small-text pl-2 pr-2"><?php echo $brojVilaDestinacija ?></div>
                    </a>
                    <?php endif; ?>
                    <?php } //endforeach ?>
                    <!-- <?php echo '    Broj pod stranica :'. $count ?> -->
                    <div class="single-item">
                        <a href="<?php the_field('btn_link') ?>"
                            class="second-btn w-100 mb-2"><?php the_field('btn_text') ?>Pogledajte celokupnu ponudu</a>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</div>
<?php endwhile; endif;  ?>
<?php get_footer(); ?>