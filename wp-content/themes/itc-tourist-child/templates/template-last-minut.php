<?php

/**
 * Template Name: Last minut
 *
 * @package TA Pluton
 */
get_header(); 

$count_posts = wp_count_posts( 'lastminut' )->publish;

?>


<div class="special-header-image pt-5" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/<?php the_field('sezona_special'); ?>.png'),url('<?php the_field('slika_special'); ?>')">

	<div class="container d-flex justify-content-between">

		<div class="d-flex flex-column title">
			<h1 class="text-uppercase"><?php the_title(); ?></h1>
			<div class="star-rating">
				<span><?php echo $count_posts ?> ponuda</span>
			</div>
		</div>

		<?php if (get_field('id_video_special')) : ?>
			<div class="video-container d-flex justify-content-center align-items-center <?php echo $noHeaderClass; ?>" data-toggle="modal" data-target="#openVideo">
				<div class="icon-container">
					<div class="pulse"></div>

					<div class="icon-border <?php echo $noHeaderClass; ?>">
						<div class="icon <?php echo $noHeaderClass; ?>">
							<i class="icon-play <?php echo $noHeaderClass; ?>"></i>
						</div>
					</div>
				</div>
				<span class="ml-3">Pogledaj video</span>


			</div>
		<?php endif; ?>

	</div>

</div>


<!-- Modal -->
<div class="modal fade" id="openVideo" tabindex="-1" role="dialog" aria-labelledby="openVideoTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="openVideoTitle"><?php the_field('naslov_videa_special') ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php the_field('id_video__special') ?>?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
			</div>
		</div>
	</div>
</div>

<?php
$current_date = date('Y-m-d');
$posts = get_posts(array(
	"post_type" => "lastminut",
	"post_status" => "publish",
	"posts_per_page" => -1,
	'meta_query' => array(
		array(
		  'key' => 'vazi_do_datuma_last',
		  'value' => $current_date,
		  'type' => 'DATE',
		  'compare' => '>='
		)
	  ),
));
?>
<div class="container">
	<div class="row pt-5 pb-5">
		<?php if ($posts && count($posts) > 0) : global $post; ?>
			<?php foreach ($posts as $post) : setup_postdata($post);
				$featured_post = get_field('smestaj_last');
			?>
				<div class="col-12">
					<div class="info-card minut-card d-flex align-items-center justify-content-between mb-4">
						<div class="d-flex flex-column">
							<div class="title d-flex align-items-center">
								<h4><?php echo get_the_title($featured_post); ?></h4>
								<span class="d-flex align-items-center main-color-text font-weight-bold">
									<i class="icon-pin mt-1 mr-2 ml-3 main-color-text"></i>
								<?php
								$parent = wp_get_post_parent_id($featured_post);
								$grandparent = wp_get_post_parent_id($parent);
								echo get_the_title($parent) . ', ' . get_the_title($grandparent);
								?>
								</span>
							</div>
							<p><?php the_field('kratak_opis_last') ?></p>
						</div>
						<div class="fm-info d-flex align-items-center">
							<?php if (get_field('tip_prevoza', $featured_post)) : ?>
							<div class="prevoz d-flex flex-column mr-5">
								<span>Prevoz</span>
								
									<h5 class="d-flex">
										<?php
										$tipPrevoza = get_field('tip_prevoza', $featured_post);
										if ($tipPrevoza) : ?>
											<?php foreach ($tipPrevoza  as $prevoz) : ?>
												<i class="main-color-text mr-2 icon-<?php echo $prevoz['value']; ?>" data-toggle="tooltip" data-placement="right" title="<?php echo $prevoz['label']; ?>"></i>
											<?php endforeach; ?>
										<?php endif; ?>
									</h5>
							</div>
							<?php endif; ?>
							<div class="vazi-do mr-5">
								<span>Važi do</span>
								<h5><?php 
								$date = date_create(get_field('vazi_do_datuma_last'));
								echo date_format($date,"d.m.Y"); 
								?></h5>
							</div>
							<div class="cena mr-5">
								<span>Cena</span>
								<h5 class="main-color-text"><?php the_field('nova_cena_last') ?> <del><?php the_field('stara_cena_last') ?></del></h5>
							</div>
							<a href="<?php echo get_permalink($featured_post); ?>" class="second-btn">Vidi više</a>
						</div>
					</div>
				</div>
			<?php endforeach;
			wp_reset_postdata(); ?>
		<?php endif; ?>
	</div>
</div>



<?php get_footer(); ?>