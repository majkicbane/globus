<?php
/**
 * Template Name: 3. dubina - Prikaz objekata
 *
 * @package TA Pluton
 */
get_header();
//require('function/search-get.php');
$destSezona = get_field('sezona');
global $wp;
$current_url = home_url(add_query_arg(array(), $wp->request));
?>
<!-- Ako ima header slika -->
<script type="text/javascript">
</script>
<?php
global $post;
$currentID = $post->ID;
$pages_no = get_pages("child_of=$currentID&depth=1&parent=$currentID");
$count = count($pages_no);
?>
<div id="header-image">
    <?php if (get_field('header_image')) : ?>
    <img src="<?php the_field('header_image'); ?>" class="object-fit-center" />
    <?php $noHeaderClass = '' ?>
    <?php else : ?>
    <img src="<?php the_field('pozadina_drzavadestinacija', 'option'); ?>" class="object-fit-center" />
    <?php $noHeaderClass = 'no-header-class' ?>
    <?php endif; ?>
    <div class="white-bg"></div>
    <div class="container position-absolute d-flex justify-content-between">
        <div class="d-flex flex-column title">
            <?php if (get_field('iframe_ponude')) : ?>
            <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
            <?php else : ?>
            <?php
				if ($count == 1) {
					the_title('<h1 class="entry-title text-uppercase ' . $noHeaderClass . '">', '</h1> <span class="br-destinacija">' . $count . ' objekat u ponudi</span>');
				} else if ($count > 1 && $count < 5) {
					the_title('<h1 class="entry-title text-uppercase ' . $noHeaderClass . '">', '</h1> <span class="br-destinacija">' . $count . ' objekta u ponudi</span>');
				} else {
					the_title('<h1 class="entry-title text-uppercase ' . $noHeaderClass . '">', '</h1> <span class="br-destinacija">' . $count . ' objekata u ponudi</span>');
				} ?>
            <?php endif; ?>
            <?php if (function_exists('yoast_breadcrumb')) {
				yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
			}
			?>
        </div>
        <?php if (get_field('embed_video')) : ?>
        <div class="video-container d-flex justify-content-center align-items-center <?php echo $noHeaderClass; ?>"
            data-toggle="modal" data-target="#openVideo">
            <div class="icon-container">
                <div class="pulse"></div>
                <div class="icon-border <?php echo $noHeaderClass; ?>">
                    <div class="icon <?php echo $noHeaderClass; ?>">
                        <i class="icon-play <?php echo $noHeaderClass; ?>"></i>
                    </div>
                </div>
            </div>
            <span class="ml-3">Pogledaj video</span>
            <!-- Modal -->
            <div class="modal fade" id="openVideo" tabindex="-1" role="dialog" aria-labelledby="openVideoTitle"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="openVideoTitle"><?php the_field('naslov_video_klipa') ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <iframe width="100%" height="315"
                                src="https://www.youtube.com/embed/<?php the_field('embed_video') ?>?rel=0"
                                frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php if (get_field('iframe_ponude')) : ?>
<div class="container">
    <?php the_field('iframe_ponude'); ?>
</div>
<?php else : ?>
<div class="container mb-5">
    <div class="row">
        <div id="primary" class="col-xl-9 col-lg-8 col-sm-12">
            <?php if (get_field('kraci_opis')) : ?>
            <div id="expandable">
                <div class="card-header" id="headingOne">
                    <p><?php the_field('kraci_opis') ?></p>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#expandable">
                    <div class="card-body">
                        <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part('template-parts/content/content', 'page'); ?>
                        <?php $parent = $post->ID;
									?>
                        <?php endwhile; // end of the loop. 
								?>
                    </div>
                </div>
                <span class="expand-btn mt-2 mb-4" data-toggle="collapse" data-target="#collapseOne"
                    aria-expanded="true" aria-controls="collapseOne">Prikaži više</span>
            </div>
            <?php endif; ?>
            <form method="get" id="destinacije-forma" class="filteri" action="<?php echo $current_url; ?>/">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php $parent = $post->ID; ?>
                <?php endwhile;
					endif; ?>
                <?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$dest = (array(
						'post_type' => 'page',
						'post_parent' => $parent,
						'posts_per_page' => 9,
						'meta_query' => $dodaci,
						'order' => $order,
						'orderby' => $orderby,
						'meta_key' => $sort,
						'paged' => $paged,
						// 'meta_value' => 'Privatni',
						//    'meta_compare' => 'LIKE',
					));
					$dest = new WP_Query($dest);
					?>
                <?php if ($dest->have_posts()) : ?>
                <div class="page destinacije-wrapper-b" id="p<?php echo $paged; ?>">
                    <h3 class="text-uppercase font-weight-bold mb-4"><?php the_title(); ?> - Objekti u ponudi:</h3>
                    <div class="row destinacije destinacije-listing">
                        <?php while ($dest->have_posts()) : $dest->the_post(); ?>
                        <?php if(wp_is_mobile()) : ?>
                        <?php require('function/prikaz-objekata-mob.php'); ?>
                        <?php else : ?>
                        <?php require('function/prikaz-objekata.php'); ?>
                        <?php endif; ?>
                        <?php endwhile;
								wp_reset_postdata(); ?>
                        <?php if ($dest->max_num_pages > 1) { // check if the max number of pages is greater than 1  
								?>
                        <nav class="prev-next-posts">
                            <div class="prev-posts-link">
                                <?php echo get_next_posts_link('Older Entries', $dest->max_num_pages); // display older posts link 
											?>
                            </div>
                            <div class="next-posts-link">
                                <?php echo get_previous_posts_link('Newer Entries'); // display newer posts link 
											?>
                            </div>
                        </nav>
                        <?php } ?>
                    </div>
                </div>
                <?php endif; ?>
        </div><!-- #primary -->
        <div class="col-xl-3 col-lg-4 col-sm-12">
            <aside class="side-card mb-4">
                <?php
					global $post;
					$grandparent_get = get_post($parent);
					$grandparent = $grandparent_get->post_parent;
					$ancs = get_ancestors($post->ID, 'page');
					?>
                <div class="title">
                    <h5><?php echo get_post($ancs[0])->post_title; ?> - PONUDA</h5>
                </div>
                <div class="items">
                    <?php
						// Drzava
						$destination = $ancs[0];
						$pages_no = get_pages("child_of=$state&depth=1&parent=$state");
						$children = wp_list_pages("title_li=&child_of=" . $state . "&echo=0");
						$get_children_array = get_children($state);
						foreach ($get_children_array as $child) {
							$childID = $child->ID;
							$pageName = $child->post_title;
							// var_dump($temp);
							$pages_no2 = get_pages("child_of=$childID&depth=1&parent=$childID");
							$brojVilaDestinacija = count($pages_no2);
						?>
                    <?php if ($brojVilaDestinacija != 'empty') : ?>
                    <a href="" class="d-flex single-item justify-content-between align-items-center">
                        <h6 class="text-uppercase m-0"><?php echo $pageName ?></h6>
                        <div class="badge small-text pl-2 pr-2"><?php echo $brojVilaDestinacija ?></div>
                    </a>
                    <?php endif; ?>
                    <?php } //endforeach 
						?>
                    <div class="single-item">
                        <a href="<?php the_field('btn_link') ?>"
                            class="second-btn w-100 mb-2"><?php the_field('btn_text') ?>Pogledajte celokupnu ponudu</a>
                    </div>
                </div>
            </aside>
            <aside class="side-card-map mb-4">
                <?php
					if (get_field('mapa_all_dest')) {
						$form =  get_field('mapa_all_dest');
						echo do_shortcode($form);
					}
					?>
            </aside>
            <?php if (have_rows('novogodisnje_ponude_kartica','option')) : ?>
            <aside class="side-card side-card-special">
                <div id="particles-js">
                </div>
                <div class="title">
                    <h5><?php the_field('naslov_kartice_ng','option') ?></h5>
                </div>
                <div class="items">
                    <?php while (have_rows('novogodisnje_ponude_kartica','option')) : the_row(); ?>
                    <?php $featured_post = get_sub_field('ponuda','option'); ?>
                    <a href="<?php the_permalink($featured_post); ?>"
                        class="single-item d-flex flex-column justify-content-between">
                        <div class="top d-flex justify-content-between">
                            <div class="d-flex align-items-center">
                                <h6 class="d-inline-block mr-1 text-uppercase">
                                    <?php the_sub_field('naslov_ponude','option'); ?></h6>
                                <span class="badge small-text"><?php the_sub_field('tekst_bedza','option'); ?></span>
                            </div>
                            <h6 class="d-flex align-items-center font-weight-bold">
                                <span class="small-badge small-badge-left d-flex ">
                                    <?php
											$tipPrevoza = get_field('tip_prevoza_spec', $featured_post);
											if ($tipPrevoza) : ?>
                                    <?php foreach ($tipPrevoza  as $prevoz) : ?>
                                    <i class="mr-2 icon-<?php echo $prevoz['value']; ?>" data-toggle="tooltip"
                                        data-placement="right" title="<?php echo $prevoz['label']; ?>"></i>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </span>
                                <?php the_field('cena_grad_evrope',$featured_post); ?>
                            </h6>
                        </div>
                        <div class="bottom d-flex justify-content-between">
                            <span class="small-text d-block">Polazak:
                                <?php the_field('mesto_polaska', $featured_post) ?></span>
                            <span class="small-text"><?php the_sub_field('nocenja'); ?></span>
                        </div>
                    </a>
                    <?php endwhile; ?>
                    <div class="single-item">
                        <a href="<?php the_field('link_dugmeta_ng','option') ?>"
                            class="second-btn w-100 mb-2"><?php the_field('naslov_dugmeta_ng','option') ?></a>
                    </div>
                </div>
            </aside>
            <?php endif; ?>
        </div>
        </form>
    </div>
</div> <!-- container -->
<?php endif; ?>
<?php get_footer(); ?>