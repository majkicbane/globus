<?php

/**
 * Template Name: Single - Avio karte
 *
 * @package TA Pluton
 */
get_header(); ?>


<!-- Posalji upit kartica -->
<button class="inquiry-btn"><i class="icon-email"></i></button>

<div class="inquiry-card">
	<h5 class="title">POŠALJITE UPIT</h5>
	<div class="inquiry-form">
		<?php $form =  get_field('forma_za_upit', 'options') ?>
		<?php echo do_shortcode($form); ?>
	</div>
</div>



<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="header-image avio-karte" style="background-image: url('<?php the_field('avio_karte_pozadina', 'option'); ?>')">
			<div class="container">
				<div class="row">
					<div class="col-12 d-flex justify-content-between align-items-center top-head-title">

						<div class="d-flex align-items-start hotel-title">
							<h2 class="mb-0 d-flex flex-column">
								<?php the_title();  ?>
							</h2>


						</div>
						<div class="d-flex align-items-center hotel-share">
							<span>Podeli: </span>
							<?php require('function/share-horizontal.php');
							?>
						</div>

					</div>
					<div class="col-xl-7 col-lg-6 col-sm-12 <?php if (get_field('sakrij_letove') == true) : ?>col-xl-9 col-lg-8 <?php endif; ?>">

						<?php
						$slika = get_field('galerija');
						$size = 'full'; // (thumbnail, medium, large, full or custom size)
						?>
						<?php if (get_field('galerija')) : ?>
							<div id="sync1" class="slider owl-carousel mb-3">
								<?php if ($slika) : ?>
									<?php foreach ($slika as $image_id) : ?>
										<div class="item">
											<?php echo wp_get_attachment_image($image_id, $size, ["class" => "object-fit-center border-radius-mobile"]); ?>
										</div>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>
							<div id="sync2" class="navigation-thumbs owl-carousel">
								<?php if ($slika) : ?>
									<?php foreach ($slika as $image_id) : ?>
										<div class="item">
											<?php echo wp_get_attachment_image($image_id, $size, ["class" => "object-fit-center border-radius-mobile"]); ?>
										</div>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>
						<?php else : ?>
							<div style="background-image:url('<?php echo get_stylesheet_directory_uri(); ?>/img/noimg.png')" class="no-img"></div>
						<?php endif; ?>
					</div>
					<div class="col-xl-5 col-lg-6 col-sm-12 <?php if (get_field('sakrij_letove') == true) : ?>d-none<?php endif; ?>">
						<div class="info-card d-flex justify-content-center flex-column align-items-center">
							<div class="logo-kompanije">
								<?php
								$image = get_field('prevoznik');
								$size = 'medium'; // (thumbnail, medium, large, full or custom size)
								if ($image) {
									echo wp_get_attachment_image($image, $size);
								}
								?>
							</div>
							<?php if (have_rows('let')) : ?>
								<?php while (have_rows('let')) : the_row(); ?>
									<div class="let">
										<div class="od-do">
											<h2><?php the_field('od_destinacija'); ?></h2>
											<div class="airport">
												<i class="icon-surface1 main-color-text"></i>
											</div>
											<h2><?php the_field('do_destinacija'); ?></h2>
										</div>
										<div class="let-info">
											<section class="d-flex flex-column">
												<span>Dan(i)</span>
												<span class="font-weight-bold"><?php the_sub_field('dan'); ?></span>
											</section>
											<section class="d-flex flex-column">
												<span>Polazak</span>
												<span class="font-weight-bold"><?php the_sub_field('polazak'); ?></span>
											</section>
											<section class="d-flex flex-column">
												<span>Dolazak</span>
												<span class="font-weight-bold"><?php the_sub_field('dolazak'); ?></span>
											</section>
											<section class="d-flex flex-column w-cena">
												<span>Cena</span>
												<span class="font-weight-bold"><?php the_sub_field('cena'); ?> <i class="icon-information medium-text main-color-text" data-toggle="tooltip" data-placement="top" title="Cena karte je promenjiva"></i> </span>
											</section>
										</div>
										<?php if (get_sub_field('povratni_let') == true) : ?>
											<div class="od-do">
												<h4><?php the_field('do_destinacija'); ?></h4>
												<div class="airport">
													<i class="icon-surface1 main-color-text"></i>
												</div>
												<h4><?php the_field('od_destinacija'); ?></h4>
											</div>
											<div class="let-info w-75">
												<section class="d-flex flex-column">
													<span>Dan(i)</span>
													<span class="font-weight-bold"><?php the_sub_field('dan_povratnog_leta'); ?></span>
												</section>
												<section class="d-flex flex-column">
													<span>Polazak</span>
													<span class="font-weight-bold"><?php the_sub_field('polazak_povratni'); ?></span>
												</section>
												<section class="d-flex flex-column">
													<span>Dolazak</span>
													<span class="font-weight-bold"><?php the_sub_field('dolazak_povratni'); ?></span>
												</section>
											</div>
										<?php endif; ?>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
					<?php if (get_field('sakrij_letove') == true) : ?>
						<div class="col-xl-3 col-lg-4 col-sm-12">
							<aside class="side-card mb-4">
								<?php
								$grandparent_get = get_post($parent);
								$grandparent = $grandparent_get->post_parent;
								$ancs = get_ancestors($post->ID, 'page');
								?>
								<div class="title">
									<h5><?php echo get_post($ancs[1])->post_title; ?> - PONUDA</h5>
								</div>
								<div class="items">
									<?php
									global $post;
									$currentID = $post->ID;
									$parentPage = $post->post_parent;

									// Drzava
									$state = $ancs[1];
									$pages_no = get_pages("child_of=$state&depth=1&parent=$state");

									// Broj destinacija u drzavi
									$count = count($pages_no);

									$children = wp_list_pages("title_li=&child_of=" . $state . "&echo=0");

									$get_children_array = get_children($state);
									foreach ($get_children_array as $child) {
										$childID = $child->ID;
										$pageName = $child->post_title;
										// var_dump($temp);
										$pages_no2 = get_pages("child_of=$childID&depth=1&parent=$childID");
										$brojVilaDestinacija = count($pages_no2);


										// echo $get_children_array;

									?>
										<?php if ($brojVilaDestinacija != 'empty') : ?>
											<a href="<?php the_permalink($childID) ?>" class="d-flex single-item justify-content-between align-items-center">
												<h6 class="text-uppercase m-0"><?php echo $pageName ?></h6>
												<div class="badge small-text pl-2 pr-2"><?php echo $brojVilaDestinacija ?></div>
											</a>
										<?php endif; ?>
									<?php } //endforeach 
									?>
									<!-- <?php echo '    Broj pod stranica :' . $count ?> -->
									<div class="single-item">
										<a href="<?php the_field('btn_link') ?>" class="second-btn w-100 mb-2"><?php the_field('btn_text') ?>Pogledajte celokupnu ponudu</a>
									</div>
								</div>
							</aside>
							<?php
				$posts = get_posts(array(
					"post_type"=>"aviokarte",
					"post_status"=>"publish",
					"posts_per_page"=>-1 
				));
				?>

				<?php if ($posts && count($posts)>0) : global $post; ?>
					<aside class="side-card ">
					<div class="title">
						<h5 class=" card-primary">Avio karte</h5>
					</div>
					<div class="items">
						<?php foreach ($posts as $post) : setup_postdata($post); ?>
							<a href="" class="d-flex single-item justify-content-between align-items-center">
								<div class="d-flex align-items-center justify-content-center">
									<h6 class="m-0"><?php the_field('od_grada'); ?></h6>
									<div class="airport">
										<i class="icon-surface1 main-color-text"></i>
									</div>
									<h6 class="m-0"><?php the_field('do_grada'); ?></h6>
								</div>
								<div class="main-color-text"><?php the_field('cena_aviokarte'); ?></div>
							</a>
							
						<?php endforeach; wp_reset_postdata(); ?>
						<div class="single-item">
							<a href="<?php echo get_site_url(); ?>/globus/avio-karte/" class="second-btn w-100 mb-2">Pogledajte celokupnu ponudu</a>
						</div>
					</div>
					</aside>
				<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="container mt-5 mb-5">
			<div class="row">
				<div class="col-xl-9 col-lg-8 col-sm-12 ">
					<div class="hotel-tabs">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item w-fit-content">
								<a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true"><i class="icon-information"></i> INFORMACIJE</a>
							</li>
						</ul>
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">

								<div class="d-flex justify-content-between align-items-center info-tab-top">
									<?php
									$tipPrevoza = get_field('tip_prevoza');
									if ($tipPrevoza) : ?>
										<div class="tip-prevoza">
											<?php foreach ($tipPrevoza  as $prevoz) : ?>
												<div class="icon">
													<i class="icon-<?php echo $prevoz['value']; ?>"></i>
													<span><?php echo $prevoz['label']; ?></span>
												</div>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>

									<?php
									$uslugaHotela = get_field('usluga_hotela');
									if ($uslugaHotela) : ?>
										<div class="usluga-hotela">
											<?php foreach ($uslugaHotela  as $usluga) : ?>
												<div class="icon">
													<span data-toggle="tooltip" data-placement="top" title="Tooltip on top"><?php echo $usluga['label']; ?></span>
												</div>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>
								</div>


								<?php the_content(); ?>

							</div>

						</div>
						<div class="tab-pane">
							<div id="includesOrNot">
								<?php if (get_field('obuhvata')) : ?>
									<div class="item">
										<div class="card-header" id="headingObuhvata" data-toggle="collapse" data-target="#collapseObuhvata" aria-expanded="true" aria-controls="collapseObuhvata">
											<h5 class="mb-0 d-flex align-items-center justify-content-between">
												<button class="btn btn-link">
													AranŽman obuhvata:
												</button>
												<i class="icon-arrow-down"></i>
											</h5>
										</div>

										<div id="collapseObuhvata" class="collapse " aria-labelledby="headingObuhvata" data-parent="#includesOrNot">
											<div class="card-body">
												<?php the_field('obuhvata') ?>
											</div>
										</div>
									</div>
								<?php endif; ?>
								<?php if (get_field('neobuhvata')) : ?>
									<div class="item">
										<div class="card-header" id="headingNeObuhvata" data-toggle="collapse" data-target="#collapseNeObuhvata" aria-expanded="false" aria-controls="collapseNeObuhvata">
											<h5 class="mb-0 d-flex align-items-center justify-content-between">
												<button class="btn btn-link collapsed">
													AranŽman NE obuhvata:
												</button>
												<i class="icon-arrow-down"></i>
											</h5>
										</div>
										<div id="collapseNeObuhvata" class="collapse" aria-labelledby="headingNeObuhvata" data-parent="#includesOrNot">
											<div class="card-body">
												<?php the_field('neobuhvata') ?>
											</div>
										</div>
									</div>
								<?php endif; ?>
							</div>


						</div>

					</div>


				</div>
				<div class="col-xl-3 col-lg-4 col-sm-12 <?php if (get_field('sakrij_letove') == true) : ?>d-none<?php endif; ?>">
					<aside class="side-card mb-4">
						<?php
						$grandparent_get = get_post($parent);
						$grandparent = $grandparent_get->post_parent;
						$ancs = get_ancestors($post->ID, 'page');
						?>
						<div class="title">
							<h5><?php echo get_post($ancs[1])->post_title; ?> - PONUDA</h5>
						</div>
						<div class="items">
							<?php
							global $post;
							$currentID = $post->ID;
							$parentPage = $post->post_parent;

							// Drzava
							$state = $ancs[1];
							$pages_no = get_pages("child_of=$state&depth=1&parent=$state");

							// Broj destinacija u drzavi
							$count = count($pages_no);

							$children = wp_list_pages("title_li=&child_of=" . $state . "&echo=0");

							$get_children_array = get_children($state);
							foreach ($get_children_array as $child) {
								$childID = $child->ID;
								$pageName = $child->post_title;
								// var_dump($temp);
								$pages_no2 = get_pages("child_of=$childID&depth=1&parent=$childID");
								$brojVilaDestinacija = count($pages_no2);


								// echo $get_children_array;

							?>
								<?php if ($brojVilaDestinacija != 'empty') : ?>
									<a href="<?php the_permalink($childID) ?>" class="d-flex single-item justify-content-between align-items-center">
										<h6 class="text-uppercase m-0"><?php echo $pageName ?></h6>
										<div class="badge small-text pl-2 pr-2"><?php echo $brojVilaDestinacija ?></div>
									</a>
								<?php endif; ?>
							<?php } //endforeach 
							?>
							<!-- <?php echo '    Broj pod stranica :' . $count ?> -->
							<div class="single-item">
								<a href="<?php the_field('btn_link') ?>" class="second-btn w-100 mb-2"><?php the_field('btn_text') ?>Pogledajte celokupnu ponudu</a>
							</div>
						</div>
					</aside>
					<?php
				$posts = get_posts(array(
					"post_type"=>"aviokarte",
					"post_status"=>"publish",
					"posts_per_page"=>-1 
				));
				?>

				<?php if ($posts && count($posts)>0) : global $post; ?>
					<aside class="side-card ">
					<div class="title">
						<h5 class=" card-primary">Avio karte</h5>
					</div>
					<div class="items">
						<?php foreach ($posts as $post) : setup_postdata($post); ?>
							<a href="" class="d-flex single-item justify-content-between align-items-center">
								<div class="d-flex align-items-center justify-content-center">
									<h6 class="m-0"><?php the_field('od_grada'); ?></h6>
									<div class="airport">
										<i class="icon-surface1 main-color-text"></i>
									</div>
									<h6 class="m-0"><?php the_field('do_grada'); ?></h6>
								</div>
								<div class="main-color-text"><?php the_field('cena_aviokarte'); ?></div>
							</a>
							
						<?php endforeach; wp_reset_postdata(); ?>
						<div class="single-item">
							<a href="<?php echo get_site_url(); ?>/globus/avio-karte/" class="second-btn w-100 mb-2">Pogledajte celokupnu ponudu</a>
						</div>
					</div>
					</aside>
				<?php endif; ?>
				</div>
			</div>
		</div>
<?php endwhile;
endif;  ?>
<?php get_footer(); ?>