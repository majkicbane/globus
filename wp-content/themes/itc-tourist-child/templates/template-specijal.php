<?php

/**
 * Template Name: 4. dubina - Objekat (Evropski gradovi, Nova godina...)
 *
 * @package TA Pluton
 */
get_header(); ?>


<!-- Posalji upit kartica -->
<button class="inquiry-btn"><i class="icon-email"></i></button>
<a class="inquiry-viber-btn" href="viber://chat/?number=%2B381604150513"><i class="icon-viber"></i></a>


<div class="inquiry-card">
	<h5 class="title">POŠALJITE UPIT</h5>
	<p class="napomena-forma">Slanjem ove forme saljete upit za: <br> <strong><?php the_title(); ?></strong></p>
	<div class="inquiry-form">
		<?php $form =  get_field('forma_za_upit', 'options') ?>
		<?php echo do_shortcode($form); ?>
	</div>

</div>

<?php if (get_field('sezona_special') == 'mask-special-winter') : ?>
	<div id="particles-js">
		<!-- Snow -->
	</div>
<?php endif; ?>

<div class="special-header-image pt-5" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/<?php the_field('sezona_special'); ?>.png'),url('<?php the_field('slika_special'); ?>')">

	<div class="container d-flex justify-content-between">

		<div class="d-flex flex-column title">
			<h1><?php the_title(); ?></h1>
			<div class="star-rating">
				Cena <?php the_field('cena_grad_evrope'); ?>
			</div>
		</div>

		<?php if (get_field('id_video_special')) : ?>
			<div class="video-container d-flex justify-content-center align-items-center <?php echo $noHeaderClass; ?>" data-toggle="modal" data-target="#openVideo">
				<div class="icon-container">
					<div class="pulse"></div>

					<div class="icon-border <?php echo $noHeaderClass; ?>">
						<div class="icon <?php echo $noHeaderClass; ?>">
							<i class="icon-play <?php echo $noHeaderClass; ?>"></i>
						</div>
					</div>
				</div>
				<span class="ml-3">Pogledaj video</span>


			</div>
		<?php endif; ?>

	</div>

</div>


<!-- Modal -->
<div class="modal fade" id="openVideo" tabindex="-1" role="dialog" aria-labelledby="openVideoTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="openVideoTitle"><?php the_field('naslov_videa_special') ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php the_field('id_video__special') ?>?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
			</div>
		</div>
	</div>
</div>



<div class="header-image">
	<div class="container">
		<div class="row">
			<div class="col-12 d-flex justify-content-end align-items-end mb-3 top-head-title">

				<div class="d-flex align-items-center hotel-share">
					<span>Podeli: </span>
					<?php require('function/share-horizontal.php'); ?>
				</div>

			</div>
			<div class="col-xl-9 col-lg-8 col-sm-12 position-relative">
				<?php if(get_field('novogodisnja_ponuda')==true): ?>
					<img class="santa-hat" src="<?php echo get_stylesheet_directory_uri(); ?>/img/santa-hat.svg">
				<?php endif; ?>
				<?php
				$slika = get_field('galerija');
				$size = 'full'; // (thumbnail, medium, large, full or custom size)
				?>
				<?php if (get_field('galerija')) : ?>
					<div id="sync1" class="slider owl-carousel mb-3">
						<?php if ($slika) : ?>
							<?php foreach ($slika as $image_id) : ?>
								<div class="item">
									<?php echo wp_get_attachment_image($image_id, $size, ["class" => "object-fit-center border-radius-mobile"]); ?>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
					<div id="sync2" class="navigation-thumbs owl-carousel">
						<?php if ($slika) : ?>
							<?php foreach ($slika as $image_id) : ?>
								<div class="item">
									<?php echo wp_get_attachment_image($image_id, $size, ["class" => "object-fit-center border-radius-mobile"]); ?>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				<?php else : ?>
					<div style="background-image:url('<?php echo get_stylesheet_directory_uri(); ?>/img/noimg.png')" class="no-img"></div>
				<?php endif; ?>


				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="hotel-tabs mt-5 mb-5">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true"><i class="icon-information"></i> INFORMACIJE</a>
								</li>
								<?php if (get_field('program_putovanja_dani')) : ?>
									<li class="nav-item">
										<a class="nav-link" id="program-tab" data-toggle="tab" href="#program" role="tab" aria-controls="program" aria-selected="false"><i class="icon-journey"></i> PROGRAM PUTOVANJA</a>
									</li>
								<?php endif; ?>
								<?php if (get_field('cenovnik')) : ?>
									<li class="nav-item">
										<a class="nav-link" id="price-tab" data-toggle="tab" href="#price" role="tab" aria-controls="price" aria-selected="false"><i class="icon-wallet"></i> CENOVNIK</a>
									</li>
								<?php endif; ?>
								<?php if (get_field('mapa_vile')) : ?>
									<li class="nav-item">
										<a class="nav-link" id="map-tab" data-toggle="tab" href="#map" role="tab" aria-controls="map" aria-selected="false"><i class="icon-place"></i> Mapa</a>
									</li>
								<?php endif; ?>
							</ul>
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">

									<div class="d-flex justify-content-between align-items-center info-tab-top">
										<?php
										$tipPrevoza = get_field('tip_prevoza_spec');
										if ($tipPrevoza) : ?>
											<div class="tip-prevoza">
												<?php foreach ($tipPrevoza  as $prevoz) : ?>
													<div class="icon">
														<i class="icon-<?php echo $prevoz['value']; ?>"></i>
														<span><?php echo $prevoz['label']; ?></span>
													</div>
												<?php endforeach; ?>
											</div>
										<?php endif; ?>

										<div class="usluga-hotela">
											<div class="icon">
												<span data-toggle="tooltip" data-placement="top" title="Tooltip on top"><strong>Mesto polaska: </strong><?php the_field('mesto_polaska'); ?></span>
											</div>
										</div>
									</div>


									<?php the_content(); ?>

								</div>
								<div class="tab-pane fade" id="program" role="tabpanel" aria-labelledby="program-tab">
									<?php if (have_rows('program_putovanja_dani')) : ?>
										<?php while (have_rows('program_putovanja_dani')) : the_row(); ?>

											<div class="d-flex program-sadrzaj">
												<div class="day">
													<div class="number">
														<span>
															<?php the_sub_field('dan'); ?>
														</span>
														<span>dan</span>
													</div>
													<div class="step-line"></div>
												</div>
												<div class="content">
													<h5><?php the_sub_field('naslov'); ?></h5>
													<p><?php the_sub_field('sadrzaj'); ?></p>
												</div>
											</div>

										<?php endwhile; ?>
									<?php endif; ?>
								</div>
								<div class="tab-pane fade" id="price" role="tabpanel" aria-labelledby="price-tab">
									<?php the_field('cenovnik') ?>
								</div>
								<div class="tab-pane fade" id="map" role="tabpanel" aria-labelledby="map-tab">
									<?php the_field('mapa_vile') ?>
								</div>

							</div>
							<div class="tab-pane">
								<div id="includesOrNot">
									<?php if (get_field('obuhvata')) : ?>
										<div class="item">
											<div class="card-header" id="headingObuhvata" data-toggle="collapse" data-target="#collapseObuhvata" aria-expanded="true" aria-controls="collapseObuhvata">
												<h5 class="mb-0 d-flex align-items-center justify-content-between">
													<button class="btn btn-link">
														AranŽman obuhvata:
													</button>
													<i class="icon-arrow-down"></i>
												</h5>
											</div>

											<div id="collapseObuhvata" class="collapse " aria-labelledby="headingObuhvata" data-parent="#includesOrNot">
												<div class="card-body">
													<?php the_field('obuhvata') ?>
												</div>
											</div>
										</div>
									<?php endif; ?>
									<?php if (get_field('neobuhvata')) : ?>
										<div class="item">
											<div class="card-header" id="headingNeObuhvata" data-toggle="collapse" data-target="#collapseNeObuhvata" aria-expanded="false" aria-controls="collapseNeObuhvata">
												<h5 class="mb-0 d-flex align-items-center justify-content-between">
													<button class="btn btn-link collapsed">
														AranŽman NE obuhvata:
													</button>
													<i class="icon-arrow-down"></i>
												</h5>
											</div>
											<div id="collapseNeObuhvata" class="collapse" aria-labelledby="headingNeObuhvata" data-parent="#includesOrNot">
												<div class="card-body">
													<?php the_field('neobuhvata') ?>
												</div>
											</div>
										</div>
									<?php endif; ?>
								</div>

								<!-- Cene fakultativnih izleta -->
								<!-- <?php if (get_field('tekst_napomene', 'option')) : ?>
								<div id="price">
									<div class="item">
										<div class="card-header" id="headingPrice" data-toggle="collapse" data-target="#collapsePrice" aria-expanded="true" aria-controls="collapsePrice">
											<h5 class="mb-0 d-flex align-items-center justify-content-between">
												<button class="btn btn-link">
													Cene fakultativnih izleta:
												</button>
												<i class="icon-arrow-down"></i>
											</h5>
										</div>

										<div id="collapsePrice" class="collapse " aria-labelledby="headingPrice" data-parent="#price">
											<div class="card-body">
												<?php the_field('tekst_napomene', 'option') ?>
											</div>
										</div>
									</div>
								</div>
							<?php endif; ?> -->


							</div>

						</div>



				<?php endwhile;
				endif;  ?>



			</div>
			<div class="col-xl-3 col-lg-4 col-sm-12">
				<?php
				$posts = get_posts(array(
					"post_type" => "aviokarte",
					"post_status" => "publish",
					"posts_per_page" => -1
				));
				?>

				<?php if ($posts && count($posts) > 0) : global $post; ?>
					<aside class="side-card mb-4">
						<div class="title">
							<h5 class=" card-primary">Avio karte</h5>
						</div>
						<div class="items">
							<?php foreach ($posts as $post) : setup_postdata($post); ?>
								<a href="" class="d-flex single-item justify-content-between align-items-center">
									<div class="d-flex align-items-center justify-content-center">
										<h6 class="m-0"><?php the_field('od_grada'); ?></h6>
										<div class="airport">
											<i class="icon-surface1 main-color-text"></i>
										</div>
										<h6 class="m-0"><?php the_field('do_grada'); ?></h6>
									</div>
									<div class="main-color-text"><?php the_field('cena_aviokarte'); ?></div>
								</a>

							<?php endforeach;
							wp_reset_postdata(); ?>
							<div class="single-item">
								<a href="<?php echo get_site_url(); ?>/globus/avio-karte/" class="second-btn w-100 mb-2">Pogledajte celokupnu ponudu</a>
							</div>
						</div>
					</aside>
				<?php endif; ?>
				<!-- <aside class="side-card mb-4">
						<div class="title">
							<h5><?php echo get_post($ancs[1])->post_title; ?> - PONUDA</h5>
						</div>
						<div class="items">
							<?php
							global $post;
							$currentID = $post->ID;
							$parentPage = $post->post_parent;

							// Drzava
							$state = $ancs[1];
							$pages_no = get_pages("child_of=$state&depth=1&parent=$state");

							// Broj destinacija u drzavi
							$count = count($pages_no);

							$children = wp_list_pages("title_li=&child_of=" . $state . "&echo=0");

							$get_children_array = get_children($state);
							foreach ($get_children_array as $child) {
								$childID = $child->ID;
								$pageName = $child->post_title;
								// var_dump($temp);
								$pages_no2 = get_pages("child_of=$childID&depth=1&parent=$childID");
								$brojVilaDestinacija = count($pages_no2);


								// echo $get_children_array;

							?>
								<?php if ($brojVilaDestinacija != 'empty') : ?>
									<a href="" class="d-flex single-item justify-content-between align-items-center">
										<h6 class="text-uppercase m-0"><?php echo $pageName ?></h6>
										<div class="badge small-text pl-2 pr-2"><?php echo $brojVilaDestinacija ?></div>
									</a>
								<?php endif; ?>
							<?php } //endforeach 
							?>
							<div class="single-item">
								<a href="<?php the_field('btn_link') ?>" class="second-btn w-100 mb-2"><?php the_field('btn_text') ?>Pogledajte celokupnu ponudu</a>
							</div>
						</div>
					</aside> -->
			</div>
		</div>
	</div>
</div>





<?php get_footer(); ?>