<select name="sortiranje" id="sortiranje">
    <option value="">Sortiraj</option>

	<?php if($tipSmestaja=='Hotelski') : ?>
		<option value="zvezdiceRast" <?php if($sortiranje=='zvezdiceRast') echo 'selected'; ?>>Po zvezdicama rastuće</option>
		<option value="zvezdiceOpad" <?php if($sortiranje=='zvezdiceOpad') echo 'selected'; ?>>Po zvezdicama opadajuće</option>
	<?php endif; ?>

    <?php if($sezona=='Leto') : ?>
	    <option value="plazaRast" <?php if($sortiranje=='plazaRast') echo 'selected'; ?>>Udaljenost od plaže rastuće</option>
	    <option value="plazaOpad" <?php if($sortiranje=='plazaOpad') echo 'selected'; ?>>Udaljenost od plaže opadajuće</option>

	<?php elseif ($sezona=='Zima') : ?>
	    <option value="gondolaRast" <?php if($sortiranje=='gondolaRast') echo 'selected'; ?>>Udaljenost od gondole rastuće</option>
	    <option value="gondolaOpad" <?php if($sortiranje=='gondolaOpad') echo 'selected'; ?>>Udaljenost od gondole opadajuće</option>

	<?php elseif ($sezona=='') : ?>
	    <option value="plazaRast" <?php if($sortiranje=='plazaRast') echo 'selected'; ?>>Udaljenost od plaže rastuće</option>
	    <option value="plazaOpad" <?php if($sortiranje=='plazaOpad') echo 'selected'; ?>>Udaljenost od plaže opadajuće</option>
	    <option value="gondolaRast" <?php if($sortiranje=='gondolaRast') echo 'selected'; ?>>Udaljenost od gondole rastuće</option>
	    <option value="gondolaOpad" <?php if($sortiranje=='gondolaOpad') echo 'selected'; ?>>Udaljenost od gondole opadajuće</option>
	<?php endif; ?>
    <option value="default" <?php if($sortiranje=='default') echo 'selected'; ?>>Random</option>
</select>