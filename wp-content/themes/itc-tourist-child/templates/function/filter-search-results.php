<script type="text/javascript">


    jQuery(document).ready(function(){
            var sezonaID = jQuery('#sezona').val();
            if(sezonaID){
                jQuery.ajax({
                    type:'POST',
                    url:'<?php echo get_stylesheet_directory_uri();?>/ajax-search-results.php',
                    data: { 'sezona_id' : sezonaID, 'country_id' : '<?php echo $drzava; ?>', 'samodrzava' : 'drzava' },
                    success:function(html){
                        jQuery('#country').html(html);
                    },
                    complete: function (data) {
                       var countryID = jQuery('#country').val();
                        if(countryID){
                            jQuery.ajax({
                                type:'POST',
                                url:'<?php echo get_stylesheet_directory_uri();?>/ajax-search-results.php',
                                data: { 'country_id' : countryID, 'state_id' : '<?php echo $destinacija; ?>' },
                                success:function(html){
                                    jQuery('#state').html(html);
                                }
                            });
                        }else{
                            jQuery('#state').html('<option value="">Izaberite prvo državu</option>');
                        }  
                    }
                });
            }else{
                jQuery('#country').html('<option value="">Izaberite prvo sezonu</option>');
            } 
    });
    jQuery(document).ready(function($){
        if($('#sezona').find(':selected').data('naziv-sezone')=='gradovi evrope'){
            $('#tip_smestaja').attr('disabled','disabled');
        }
        jQuery('#sezona').on('change',function(){
            $('.ucitavam').css('display', 'block');
            var sezonaID = $(this).val();
            let nazivSezone = $(this).find(':selected').data('naziv-sezone');
            
            if(nazivSezone.toLowerCase().includes("leto")){
                $('#plaza').removeClass('d-none');
                $('#gondola').addClass('d-none');
                $('#centar').addClass('d-none');

                $('#plaza select').removeAttr('disabled');
                $('#gondola select').attr('disabled','disabled');
                $('#centar select').attr('disabled','disabled');
                
                $('#tip_smestaja').removeAttr('disabled')
                $('.checkbox-filter').removeClass('d-none');
            }else if(nazivSezone.toLowerCase().includes("zimovanje")){
                $('#plaza').addClass('d-none');
                $('#centar').addClass('d-none');
                $('#gondola').removeClass('d-none');

                $('#plaza select').attr('disabled','disabled');
                $('#gondola select').removeAttr('disabled');
                $('#centar select').attr('disabled','disabled');

                $('#tip_smestaja').removeAttr('disabled');
                $('.checkbox-filter').removeClass('d-none');
            }else{
                $('#plaza').addClass('d-none');
                $('#gondola').addClass('d-none');
                $('#centar').removeClass('d-none');

                $('#plaza select').attr('disabled','disabled');
                $('#gondola select').attr('disabled','disabled');
                $('#centar select').removeAttr('disabled');

                $('#tip_smestaja').attr('disabled','disabled');
                $('.checkbox-filter').addClass('d-none');
            }

            if(sezonaID){
                jQuery.ajax({
                    type:'POST',
                    url:'<?php echo get_stylesheet_directory_uri();?>/ajax-search.php',
                    data:'sezona_id='+sezonaID,
                    success:function(html){
                    if(html.length==0) {
                        $('#country').html('<option value="" selected disabled>Nema država</option>');
                        $('.ucitavam').css('display', 'none');
                    } else {
                        $('#country').html(html);
                        $("#country option").first().attr('disabled','disabled')
                        $('.ucitavam').css('display', 'none');
                        }
                    }
                }); 
            }else{
                $('#country').html('<option value="">Izaberite prvo sezonu</option>');
            }
        });
    });


    // jQuery(document).ready(function(){
    //         var countryID = jQuery('#country').val();
    //         if(countryID){
    //             jQuery.ajax({
    //                 type:'POST',
    //                 url:'<?php echo get_stylesheet_directory_uri();?>/ajax-search-results.php',
    //                 data: { 'country_id' : countryID, 'state_id' : '<?php echo $destinacija; ?>' },
    //                 success:function(html){
    //                     console.log('123');
    //                     jQuery('#state').html(html);
    //                 }
    //             });
    //         }else{
    //             jQuery('#state').html('<option value="">Izaberite prvo državu</option>');
    //         } 
    // });
    jQuery(document).ready(function(){
        jQuery('#country').on('change',function(){
            jQuery('.ucitavam').css('display', 'block');
            var countryID = jQuery(this).val();
            if(countryID){
                jQuery.ajax({
                    type:'POST',
                    url:'<?php echo get_stylesheet_directory_uri();?>/ajax-search.php',
                    data:'country_id='+countryID,
                    success:function(html){
                    if(html.length==0) {
                        jQuery('#state').html('<option value="" selected disabled>Nema destinacija</option>');
                        jQuery('.ucitavam').css('display', 'none');
                    } else {
                        jQuery('#state').html(html);
                        jQuery("#state option").first().attr('disabled','disabled')
                        jQuery('.ucitavam').css('display', 'none');
                        }
                    }
                }); 
            }else{
                jQuery('#state').html('<option value="">Izaberite prvo državu</option>');
            }
        });
    });



</script>
    <h3>Vaša pretraga:</h3>
    <div class="row">
        <div class="col-md-12 mb-2">
            <div class="select-container">
                <i class="icon-building"></i>
                <select name="tip_smestaja" id="tip_smestaja" class="form-control">
                    <option value="apartman" <?php if($tip_smestaja=='apartman') echo 'selected'; ?>>Apartmani</option>
                    <option value="hotel" <?php if($tip_smestaja=='hotel') echo 'selected'; ?>>Hoteli</option>
                </select>
            </div>
        </div>
        <div class="col-md-12 mb-2">
            <div class="select-container">
            <i class="icon-thermometer"></i>
                <select id="sezona" name="sezona" class="form-control">
                    <option value="">Sezona</option>
                    <option value="6" <?php if($sezona=='6') echo 'selected'; ?> data-naziv-sezone="letovanje">Letovanje</option>
                    <option value="498" <?php if($sezona=='498') echo 'selected'; ?> data-naziv-sezone="zimovanje">Zimovanje</option>
                    <option value="771" <?php if($sezona=='771') echo 'selected'; ?> data-naziv-sezone="gradovi evrope">Gradovi evrope</option>
                </select>
            </div>
        </div>
        <div class="col-md-12 mb-2">
            <?php /*
                $drzave = array(
                    'post_type' => 'page',
                    'meta_key' => '_wp_page_template',
                    'post_parent' => $sezona,
                    'meta_value' => 'templates/template-drzave.php',
                    'posts_per_page' => -1
                );
            $page_template_query = new WP_Query($drzave); ?>
            <select name="country" id="country">
                <option value="">Država</option>
                <?php if(!empty($page_template_query->posts))
                {
                    foreach($page_template_query->posts as $page){
                        $id_drzave = $page->ID;
                        $title_drzave = $page->post_title;
                        $selected = '';
                        if($drzava==$id_drzave) {
                            $selected = 'selected';
                        }
                        echo $option_open;
                        echo '<option value="'. $id_drzave .'" '. $selected .'>';
                        echo $title_drzave;
                        echo '</option>';
                        $selected = '';
                    }
                }
            ?>
            </select>*/ ?>
            <div class="select-container">
                <i class="icon-flags"></i>
                <select name="country" id="country" class="form-control">
                    <option value="">Prvo sezona</option>
                </select>
            </div>
        </div>

        <div class="col-12 mb-2">
            <div class="select-container">
                <i class="icon-pin"></i>
                <select name="state" id="state" class="form-control">
                    <option value="">Prvo država</option>
                </select>
            </div>
        </div>
        <div class="mb-2 col-12">
            <div class="select-container">
                <i class="icon-sleep"></i>
                <select name="tip_smestaja_apartmana" id="tip_smestaja_apartmana" class="form-control">
                    <option value="" disabled <?php if($tipSmestajaApartmana=='') echo 'selected'; ?>>Tip smestaja</option>
                    <option value="duplex" <?php if($tipSmestajaApartmana=='duplex') echo 'selected'; ?> >Duplex</option>
                    <option value="apartman" <?php if($tipSmestajaApartmana=='apartman') echo 'selected'; ?>>Apartman</option>
                    <option value="studio" <?php if($tipSmestajaApartmana=='studio') echo 'selected'; ?>>Studio</option>
                    <option value="mezoneta" <?php if($tipSmestajaApartmana=='mezoneta') echo 'selected'; ?>>Mezoneta</option>
                </select>
            </div>

        </div>

        <div class="col-md-12 mb-2">
            <div class="select-container">
                <i class="icon-bus"></i>
                <select id="prevoz" name="tipPrevoza" class="form-control">
                    <option value="">Tip prevoza</option>
                    <option value="sports-car" <?php if($tip_prevoza=='sports-car') echo 'selected'; ?> >Sopstveni</option>
                    <option value="bus" <?php if($tip_prevoza=='bus') echo 'selected'; ?> >Autobuski</option>
                    <option value="surface1" <?php if($tip_prevoza=='surface1') echo 'selected'; ?> >Avionski</option>
                </select>
            </div>
        </div>
        <div class="col-md-12 mb-2 zalepi-dole">
            <div id="plaza" class="select-container <?php if(!($sezona=='6')) echo 'd-none' ?>">
                <i class="icon-distance"></i>
                <select name="odPlaze" id="udaljenostPlaza" class="form-control">
                    <option value="nebitno" <?php if($odPlaze=='nebitno') echo 'selected'; ?>>Udaljenost od plaže</option>
                    <option value="naPlazi" <?php if($odPlaze=='naPlazi') echo 'selected'; ?>>Na plaži</option>
                    <option value="do200" <?php if($odPlaze=='do200') echo 'selected'; ?>>Do 200m</option>
                    <option value="do400" <?php if($odPlaze=='do400') echo 'selected'; ?>>Do 400m</option>
                </select>
            </div>

            <div id="gondola" class="select-container <?php if(!($sezona=='498')) echo 'd-none' ?>">
                <i class="icon-gondola"></i>
                <select name="odGondole" id="udaljenostGondola" class="form-control">
                    <option value="nebitno" <?php if($odGondole=='nebitno') echo 'selected'; ?>>Udaljenost od gondole</option>
                    <option value="kodGondole" <?php if($odGondole=='kodGondole') echo 'selected'; ?>>Kod gondole</option>
                    <option value="do200" <?php if($odGondole=='do200') echo 'selected'; ?>>Do 200m</option>
                    <option value="do400" <?php if($odGondole=='do400') echo 'selected'; ?>>Do 400m</option>
                </select>
            </div>

            <div id="centar" class="select-container <?php if(!($sezona=='771')) echo 'd-none' ?>">
				<i class="icon-building"></i>
                <select name="odCentra" id="udaljenostCentar" class="form-control">
                    <option value="nebitno" <?php if($odCentra=='nebitno') echo 'selected'; ?>>Udaljenost od centra</option>
                    <option value="uCentru" <?php if($odCentra=='uCentru') echo 'selected'; ?>>U centru</option>
                    <option value="do200" <?php if($odCentra=='do200') echo 'selected'; ?>>Do 200m</option>
                    <option value="do400" <?php if($odCentra=='do400') echo 'selected'; ?>>Do 400m</option>
                </select>
            </div>
        </div>

        <div class="col-md-12 mb-2" id="hotelski-smestaj">
            <div class="select-container">
                <i class="icon-customer"></i>
                <select name="uslugaHotela" id="uslugaHotela" class="form-control">
                    <option value="">Usluga hotela</option>
                    <option value="NA" <?php if($uslugaHotela=='NA') echo 'selected' ;?>>NA</option>
                    <option value="ND" <?php if($uslugaHotela=='ND') echo 'selected' ;?>>ND</option>
                    <option value="PP" <?php if($uslugaHotela=='PP') echo 'selected' ;?>>PP</option>
                    <option value="PA" <?php if($uslugaHotela=='PA') echo 'selected' ;?>>PA</option>
                    <option value="ALL" <?php if($uslugaHotela=='ALL') echo 'selected' ;?>>ALL</option>
                </select>
            </div>

        </div>
        <div class="col-md-12 mb-2 filteri-ostalo d-flex align-items-center mob-filter-checkbox">
            <span class="checkbox-filter w-100">
                <a href="#" data-toggle="tooltip" title="Internet" class="d-flex flex-row w-100">
                    <input class="mb-1" type="checkbox" name="wifi" id="wifi" value="da" <?php if($wifi=='da') echo 'checked'; ?>>
                    <label for="wifi"><i class="icon-wifi"></i></label> 
                </a>
            </span>
            <span class="checkbox-filter w-100">
                <a href="#" data-toggle="tooltip" title="Televizor" class="d-flex flex-row w-100">
                    <input class="mb-1" type="checkbox" name="tv" id="tv" value="da" <?php if($tv=='da') echo 'checked'; ?>>
                    <label for="tv"><i class="icon-tv"></i></label> 
                </a>
            </span>
            <span class="checkbox-filter w-100">
                <a href="#" data-toggle="tooltip" title="Klima" class="d-flex flex-row w-100">
                    <input class="mb-1" type="checkbox" name="ac" id="ac" value="da" <?php if($ac=='da') echo 'checked'; ?>>
                    <label for="ac"><i class="icon-cool"></i></label> 
                </a>
            </span>
            <span class="checkbox-filter w-100">
                <a href="#" data-toggle="tooltip" title="Parking" class="d-flex flex-row w-100">
                    <input class="mb-1" type="checkbox" name="parking" id="parking" value="da" <?php if($parking=='da') echo 'checked'; ?>>
                    <label for="parking"><i class="icon-parking"></i></label> 
                </a>
            </span>
            <span class="checkbox-filter w-100">
                <a href="#" data-toggle="tooltip" title="Bazen" class="d-flex flex-row w-100">
                    <input class="mb-1" type="checkbox" name="bazen" id="bazen" value="da" <?php if($bazen=='da') echo 'checked'; ?>>
                    <label for="bazen"><i class="icon-swimming-pool"></i></label> 
                </a>
            </span>
            <span class="checkbox-filter w-100">
                <a href="#" data-toggle="tooltip" title="Lift" class="d-flex flex-row w-100">
                    <input class="mb-1" type="checkbox" name="lift" id="lift" value="da" <?php if($lift=='da') echo 'checked'; ?>>
                    <label for="lift"><i class="icon-elevator"></i></label> 
                </a>
            </span>
            <span class="checkbox-filter w-100">
                <a href="#" data-toggle="tooltip" title="Kućni ljubimci" class="d-flex flex-row w-100">
                    <input class="mb-1" type="checkbox" name="ljubimci" id="ljubimci" value="da" <?php if($ljubimci=='da') echo 'checked'; ?>>
                    <label for="ljubimci"><i class="icon-animals"></i></label> 
                </a>
            </span>
            <span class="checkbox-filter w-100">
                <a href="#" data-toggle="tooltip" title="Pristup za invalide" class="d-flex flex-row w-100">
                    <input class="mb-1" type="checkbox" name="pristup_invalidima" id="pristup_invalidima" value="da" <?php if($pristup_invalidima=='da') echo 'checked'; ?>>
                    <label for="pristup_invalidima"><i class="icon-disabled-person"></i></label> 
                </a>
            </span>
            <span class="checkbox-filter w-100">
                <a href="#" data-toggle="tooltip" title="Wellness" class="d-flex flex-row w-100">
                    <input class="mb-1" type="checkbox" name="wellness" id="wellness" value="da" <?php if($wellness=='da') echo 'checked'; ?>>
                    <label for="wellness"><i class="icon-flower"></i></label> 
                </a>
            </span>
        </div>
        <div class="col-md-12 mb-2">
            <button type="submit" name="submit" id="searchsubmit" class="primary-btn primary-full w-100" value=""><i class="icon-search mr-2"></i> Pretraži</button>
        </div>  
    </div>