<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#sezona').on('change', function() {
        jQuery('.ucitavam').css('display', 'block');
        var sezonaID = jQuery(this).val();
        let nazivSezone = jQuery(this).find(':selected').data('naziv-sezone');
		if(nazivSezone.toLowerCase().includes("leto")){
			jQuery('#plaza').removeClass('d-none');
			jQuery('#gondola').addClass('d-none');
            jQuery('#centar').addClass('d-none');
			jQuery('#tip_smestaja').removeAttr('disabled')
			jQuery('.checkbox-filter').removeClass('d-none');
		}else if(nazivSezone.toLowerCase().includes("zimovanje")){
			jQuery('#plaza').addClass('d-none');
            jQuery('#centar').addClass('d-none');
			jQuery('#gondola').removeClass('d-none');
			jQuery('#tip_smestaja').removeAttr('disabled');
			jQuery('.checkbox-filter').removeClass('d-none');
		}else{
			jQuery('#plaza').addClass('d-none');
			jQuery('#gondola').addClass('d-none');
            jQuery('#centar').removeClass('d-none');
			jQuery('#tip_smestaja').attr('disabled','disabled');
			jQuery('.checkbox-filter').addClass('d-none');
		}
        if (sezonaID) {
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo get_stylesheet_directory_uri();?>/ajax-search.php',
                data: 'sezona_id=' + sezonaID,
                success: function(html) {
                    if (html.length == 0) {
                        jQuery('#country').html(
                            '<option value="" selected disabled>Nema država</option>');
                        jQuery('.ucitavam').css('display', 'none');
                    } else {
                        jQuery('#country').html(html);
                        jQuery("#country option").first().attr('disabled', 'disabled')
                        jQuery('.ucitavam').css('display', 'none');
                    }
                }
            });
        } else {
            jQuery('#country').html('<option value="">Izaberite prvo sezonu</option>');
        }
    });

    jQuery('#country').on('change', function() {
        jQuery('.ucitavam').css('display', 'block');
        var countryID = jQuery(this).val();
        if (countryID) {
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo get_stylesheet_directory_uri();?>/ajax-search.php',
                data: 'country_id=' + countryID,
                success: function(html) {
                    if (html.length == 0) {
                        jQuery('#state').html(
                            '<option value="" selected disabled>Nema destinacija</option>'
                        );
                        jQuery('.ucitavam').css('display', 'none');
                    } else {
                        jQuery('#state').html(html);
                        jQuery("#state option").first().attr('disabled', 'disabled')
                        jQuery('.ucitavam').css('display', 'none');
                    }
                },
                error: function(err) {
                    jQuery('.ucitavam').css('display', 'none');
                }

            });
        } else {
            jQuery('#state').html('<option value="">Izaberite prvo državu</option>');
        }
    });
});
</script>

<form method="get" class="filteri" action="<?php echo esc_url( home_url( '/' ) ); ?>/pretraga/">
    <div class="row">
        <div class="mb-2 col-lg-3 col-md-6">
            <div class="select-container">
                <i class="icon-building"></i>
                <select name="tip_smestaja" id="tip_smestaja" class="form-control">
                    <option value="apartman">Apartman</option>
                    <option value="hotel">Hotel</option>
                </select>
            </div>

		</div>
		<div class="mb-2 col-lg-3 col-md-6">
            <div class="select-container">
                <i class="icon-thermometer"></i>
                <input type="hidden" name="sortiranje" value="">
                <input type="hidden" name="tipSmestaja" value="Privatni">
                <div class="field-group-inner w-100">
                    <?php
			    $sezona = array(
			        'post_type' => 'page',
			        'meta_key' => '_wp_page_template',
			        'meta_value' => 'templates/template-sezona.php',
			        'posts_per_page' => -1
			    );
			$page_template_query = new WP_Query($sezona); ?>
                    <span class="icon-cloud"></span>
                    <select name="sezona" id="sezona" class="control form-control">
                        <option value="" disabled selected>Sezona</option>
                        <?php if(!empty($page_template_query->posts))
				{
				    foreach($page_template_query->posts as $page){
				        $id_sezone = $page->ID;
				        $title_sezone = $page->post_title;
				        echo '<option value="'. $id_sezone .'" data-naziv-sezone="'.$title_sezone.'" >' . $title_sezone .'</option>';
				    }
				}
			?>
                    </select>
                </div>
            </div>

		</div>
		<div class="mb-2 col-lg-3 col-md-6">
            <div class="field-group-inner select-container">
                <i class="icon-flags"></i>
                <select name="country" id="country" class="control form-control">
                    <option value="">Država</option>
                    <option value="" disabled>Izaberite sezonu</option>
                </select>
            </div>
		</div>
		
        <div class="mb-2 col-lg-3 col-md-6">
            <div class="field-group-inner select-container">
                <i class="icon-pin"></i>
                <select name="state" id="state" class="control form-control">
                    <option value="">Destinacija</option>
                    <option value="" disabled>Izaberite državu</option>
                </select>
            </div>
        </div>
        <div class="mb-2 col-lg-3 col-md-6">
            <div class="select-container">
                <i class="icon-sleep"></i>
                <select name="tip_smestaja_apartmana" id="tip_smestaja_apartmana" class="form-control">
                    <option value="" disabled selected>Tip smestaja</option>
                    <option value="duplex">Duplex</option>
                    <option value="apartman">Apartman</option>
                    <option value="studio">Studio</option>
                    <option value="mezoneta">Mezoneta</option>
                </select>
            </div>

        </div>
        
        <div class="mb-2 col-lg-3 col-md-6">
            <div class="field-group-inner select-container">
                <i class="icon-bus"></i>
                <select id="prevoz" name="tipPrevoza" class="control form-control">
                    <option value="">Tip prevoza</option>
                    <option value="sports-car">Sopstveni</option>
                    <option value="bus">Autobuski</option>
                    <option value="surface1">Avionski</option>
                </select>
            </div>
        </div>
        <div class="mb-2 col-lg-3 col-md-6" id="hotelski-smestaj">
            <div class="select-container">
                <i class="icon-customer"></i>
                <select name="uslugaHotela" id="uslugaHotela" class="form-control">
                    <option value="">Usluga hotela</option>
                    <option value="NA" <?php if($uslugaHotela=='NA') echo 'selected' ;?>>NA</option>
                    <option value="ND" <?php if($uslugaHotela=='ND') echo 'selected' ;?>>ND</option>
                    <option value="PP" <?php if($uslugaHotela=='PP') echo 'selected' ;?>>PP</option>
                    <option value="PA" <?php if($uslugaHotela=='PA') echo 'selected' ;?>>PA</option>
                    <option value="ALL" <?php if($uslugaHotela=='ALL') echo 'selected' ;?>>ALL</option>
                </select>
            </div>

        </div>

        <div class="mb-2 col-lg-3 col-md-6">
            <div id="plaza" class="select-container">
                <i class="icon-distance"></i>
                <select name="odPlaze" id="udaljenostPlaza" class="form-control">
                    <option value="nebitno">Udaljenost od plaze</option>
                    <option value="naPlazi">Na plaži</option>
                    <option value="do200">Do 200m</option>
                    <option value="do400">Do 400m</option>
                </select>
            </div>
			<div id="gondola" class="select-container d-none">
				<i class="icon-gondola"></i>
                <select name="odGondole" id="udaljenostGondola" class="form-control">
                    <option value="nebitno">Udaljenost od gondole</option>
                    <option value="kodGondole">Kod gondole</option>
                    <option value="do200">Do 200m</option>
                    <option value="do400">Do 400m</option>
                </select>
            </div>
            <div id="centar" class="select-container d-none">
				<i class="icon-building"></i>
                <select name="odCentra" id="udaljenostCentar" class="form-control">
                    <option value="nebitno">Udaljenost od centra</option>
                    <option value="uCentru">U centru</option>
                    <option value="do200">Do 200m</option>
                    <option value="do400">Do 400m</option>
                </select>
            </div>
        </div>
        
        
  
        <div class="col-lg-9 d-flex align-items-center mob-filter-checkbox">
            <span class="checkbox-filter w-100">
				<a href="#" data-toggle="tooltip" title="Internet" class="d-flex">
					<input type="checkbox" name="wifi" id="wifi" value="da">
					<label for="wifi" class="m-0"><i class="icon-wifi"></i></label> 
				</a>
			</span>


            <span class="checkbox-filter w-100">
				<a href="#" data-toggle="tooltip" title="Televizor" class="d-flex">
					<input type="checkbox" name="tv" id="tv" value="da">
					<label for="tv" class="m-0"><i class="icon-tv"></i></label> 
				</a>
			</span>

            <span class="checkbox-filter w-100">
				<a href="#" data-toggle="tooltip" title="Klima" class="d-flex">
					<input type="checkbox" name="ac" id="ac" value="da">
					<label for="ac" class="m-0"><i class="icon-cool"></i></label>
				</a>
			</span>

            <span class="checkbox-filter w-100">
				<a href="#" data-toggle="tooltip" title="Parking" class="d-flex">
					<input type="checkbox" name="parking" id="parking" value="da">
					<label for="parking" class="m-0"><i class="icon-parking"></i></label>
				</a>
			</span>

            <span class="checkbox-filter w-100">
				<a href="#" data-toggle="tooltip" title="Bazen" class="d-flex">
					<input type="checkbox"name="bazen" id="bazen" value="da"> 
					<label for="bazen" class="m-0"><i class="icon-swimming-pool"></i></label>
				</a>
			</span>
			
            <span class="checkbox-filter w-100">
				<a href="#" data-toggle="tooltip" title="Lift" class="d-flex">
					<input type="checkbox" name="lift" id="lift" value="da">
					<label for="lift" class="m-0"><i class="icon-elevator"></i></label>
				</a>
			</span>

            <span class="checkbox-filter w-100">
				<a href="#" data-toggle="tooltip" title="Kućni ljubimci" class="d-flex">
					<input type="checkbox" name="ljubimci" id="ljubimci" value="da">
					<label for="ljubimci" class="m-0"><i class="icon-animals"></i></label>
				</a>
			</span>

            <span class="checkbox-filter w-100">
				<a href="#" data-toggle="tooltip" title="Pristup za invalide" class="d-flex">
					<input type="checkbox" name="pristup_invalidima" id="pristup_invalidima" value="da">
					<label for="pristup_invalidima" class="m-0"><i class="icon-disabled-person"></i></label>
				</a>
			</span>

            <span class="checkbox-filter w-100">
				<a href="#" data-toggle="tooltip" title="Wellness" class="d-flex">
					<input type="checkbox" name="wellness" id="wellness" value="da">
					<label for="wellness" class="m-0"><i class="icon-flower"></i></label> 
				</a>
			</span>

        </div>
        <div class="col-lg-3 col-md-6">
            <button type="submit" name="submit" id="searchsubmit" value="" class="primary-btn primary-full w-100">
				<i class="icon-search mr-2"></i>
				Pretraži
			</button>
        </div>
    </div>
</form>