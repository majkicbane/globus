<div class="col-xl-4 col-lg-6 col-md-6 posebno">
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"
        class="single-destinacija <?php if (get_field('tip_smestaja') == 'Privatni') : ?>single-apartman<?php elseif (get_field('tip_smestaja') == "Hotelski") : ?>single-hotel<?php endif; ?>">
        <?php
		$akcija = get_field('ima_akcija');
		if ($akcija && in_array('Da', $akcija)) : ?>
        <div class="akcija-blink blink">
            <?php the_field('tekst_akcije'); ?>
        </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-5">
                <figure>
                    <?php if (get_the_post_thumbnail()) : ?>
                    <?php the_post_thumbnail('ponude-img', array('class' => 'object-fit-center')); ?>
                    <?php else : ?>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/noimg.png" alt=""
                        class="object-fit-center">
                    <?php endif; ?>
                </figure>

                <!-- First/Last minut -->
                <?php
				$current_post_ID = get_the_ID();
				$current_date = date('Y-m-d');
				$lastmin = (array(
					"post_type" => "lastminut",
					"post_status" => "publish",
					"posts_per_page" => -1,
					'meta_query' => array(
						'reation' => 'and',
						array(
							'key' => 'vazi_do_datuma_last',
							'value' => $current_date,
							'type' => 'DATE',
							'compare' => '>='
						),
						array(
							'key' => 'smestaj_last',
							'value' => $current_post_ID,
							'compare' => 'LIKE'
						)
					),
				));
				$lastmin = new WP_Query($lastmin);
				?>
                <?php if ($lastmin->have_posts()) : ?>
                <span class="small-badge">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/last-minute.png" alt="Last minute">
                </span>
                <?php else : ?>
                <?php
					$current_post_ID = get_the_ID();
					$current_date = date('Y-m-d');
					$firstmin = (array(
						"post_type" => "firstminut",
						"post_status" => "publish",
						"posts_per_page" => -1,
						'meta_query' => array(
							'reation' => 'and',
							array(
								'key' => 'vazi_do_datuma',
								'value' => $current_date,
								'type' => 'DATE',
								'compare' => '>='
							),
							array(
								'key' => 'smestaj',
								'value' => $current_post_ID,
								'compare' => 'LIKE'
							)
						),
					));
					$firstmin = new WP_Query($firstmin);
					?>
                <?php if ($firstmin->have_posts()) : ?>
                <span class="small-badge">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/first-minute.png" alt="First minute">
                </span>
                <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="col-7 single-item-mob">

                <!-- Cena (Gradovi evrope) -->
                <?php if(get_field('cena_grad_evrope')) : ?>
                <span class="small-badge price-badge">
                    <?php the_field('cena_grad_evrope'); ?>
                </span>
                <?php endif; ?>

                <!-- Tip prevoza -->
                <div class="d-flex">
                    <span class="mr-2">Tip prevoza</span>
                    <?php if (get_field('tip_prevoza')) : ?>
                    <span class="d-flex align-items-center">
                        <?php
					$tipPrevoza = get_field('tip_prevoza');
					if ($tipPrevoza) : ?>
                        <?php foreach ($tipPrevoza  as $prevoz) : ?>
                        <i class="mr-2 icon-<?php echo $prevoz['value']; ?>" data-toggle="tooltip"
                            data-placement="right" title="<?php echo $prevoz['label']; ?>"></i>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </span>
                    <?php endif; ?>
                </div>

                <!-- Mesto polaska (Gradovi evrope) -->
                <?php if(get_field('mesto_polaska')) : ?>
                <div class="small-badge city-badge">
                    Polazak: <?php the_field('mesto_polaska'); ?>
                </div>
                <?php endif; ?>

                <div class="icons">
                    <?php
					$usluge = get_field('usluge');
					if ($usluge) : ?>
                    <?php foreach ($usluge as $usluga) :
							$labela = $usluga['value'];
							switch ($labela) {
								case "wifi":
									echo "<i class='icon-$labela'></i>";
									break;
								case "disabled-person":
									echo "<i class='icon-$labela'></i>";
									break;
								case "pool":
									echo "<i class='icon-$labela'></i>";
									break;
								case "parking-big":
									echo "<i class='icon-$labela'></i>";
									break;
								case "animals":
									echo "<i class='icon-$labela'></i>";
									break;
								case "breakfast":
									echo "<i class='icon-$labela'></i>";
									break;
								case "tv":
									echo "<i class='icon-$labela'></i>";
									break;
								default:
									echo "";
							}
						endforeach; ?>
                    <?php endif; ?>
                </div>
                <h5>
                    <?php
					$nasl = get_the_title();
					$naslov = substr($nasl, 0, 32);
					if (strlen($nasl) > 32) {
						echo $naslov . '...';
					} else {
						echo $naslov;
					}
					?>
                    <?php if (get_field('izaberi_sezonu') == 'sezona_leto') : ?>
                    <?php if (get_field('udaljenost_od_plaze')) : ?>
                    <span class="beach-distance"><i class="icon-sunbed"></i>
                        <?php the_field('udaljenost_od_plaze'); ?></span>
                    <?php endif; ?>
                    <?php else : ?>
                    <?php if (get_field('udaljenost_od_gondole')) : ?>
                    <span class="beach-distance"><i class="icon-gondola"></i>
                        <?php the_field('udaljenost_od_gondole'); ?></span>
                    <?php endif; ?>
                    <?php endif; ?>
                </h5>
            </div>
        </div>
    </a>
</div>