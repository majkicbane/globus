<script type="text/javascript">
	jQuery(document).ready(function(){
	    jQuery('#sezona2').on('change',function(){
	    	jQuery('.ucitavam').css('display', 'block');
	        var sezonaID = jQuery(this).val();
	        if(sezonaID){
	            jQuery.ajax({
	                type:'POST',
	                url:'<?php echo get_template_directory_uri();?>/ajax-search.php',
	                data:'sezona_id='+sezonaID,
	                success:function(html){
                	if(html.length==0) {
            			jQuery('#country2').html('<option value="" selected disabled>Nema država</option>');
    					jQuery('.ucitavam').css('display', 'none');
                	} else {
	                    jQuery('#country2').html(html);
	                    jQuery("#country2 option").first().attr('disabled','disabled')
	    				jQuery('.ucitavam').css('display', 'none');
		                }
		            }
	            }); 
	        }else{
	            jQuery('#country2').html('<option value="">Izaberite prvo sezonu</option>');
	        }
	    });

	    jQuery('#country2').on('change',function(){
	    	jQuery('.ucitavam').css('display', 'block');
	        var countryID = jQuery(this).val();
	        if(countryID){
	            jQuery.ajax({
	                type:'POST',
	                url:'<?php echo get_template_directory_uri();?>/ajax-search.php',
	                data:'country_id='+countryID,
	                success:function(html){
                	if(html.length==0) {
            			jQuery('#state2').html('<option value="" selected disabled>Nema destinacija</option>');
    					jQuery('.ucitavam').css('display', 'none');
                	} else {
	                    jQuery('#state2').html(html);
	                    jQuery("#state2 option").first().attr('disabled','disabled')
	    				jQuery('.ucitavam').css('display', 'none');
		                }
		            }
	            }); 
	        }else{
	            jQuery('#state2').html('<option value="">Izaberite prvo državu</option>');
	        }
	    });
	});
</script>

<form method="get" class="filteri" action="<?php echo esc_url( home_url( '/' ) ); ?>/pretraga/">
	<h3>Pretraga smeštaja u hotelima:</h3>
	<div class="row">
		<div class="col-md-2">
			<input type="hidden" name="sortiranje" value="">
			<input type="hidden" name="tipSmestaja" value="Hotelski">
			<div class="field-group-inner">
			<?php
			    $sezona = array(
			        'post_type' => 'page',
			        'meta_key' => '_wp_page_template',
			        'meta_value' => 'template-sezona.php',
			        'posts_per_page' => -1
			    );
			$page_template_query = new WP_Query($sezona); ?>
				<span class="icon-cloud"></span>
				<select name="sezona" id="sezona2" class="control">
			    <option value="" disabled selected>Sezona</option>
				<?php if(!empty($page_template_query->posts))
				{
				    foreach($page_template_query->posts as $page){
				        $id_sezone = $page->ID;
				        $title_sezone = $page->post_title;
				        echo '<option value="'. $title_sezone .'">' . $title_sezone .'</option>';
				    }
				}
			?>
			</select>
			</div>
			<div class="field-group-inner">
				<span class="icon-bus"></span>
				<select id="prevoz" name="prevoz" class="control">
				    <option value="">Tip prevoza</option>
				    <option value="Sopstveni">Sopstveni</option>
				    <option value="Autobuski">Autobuski</option>
				    <option value="Avionski">Avionski</option>
				</select>
			</div>
		</div>
		<div class="col-md-2 field-group-inner">
			<div class="field-group-inner">
			<span class="icon-earth"></span>
			<select name="country" id="country2" class="control">
			    <option value="">Država</option>
			    <option value="" disabled>Izaberite sezonu</option>
			</select>

			</div>
			<div class="field-group-inner">
			<span class="icon-placeholder-filled-point"></span>
			<select name="state" id="state2" class="control">
			    <option value="">Destinacija</option>
			    <option value="" disabled>Izaberite državu</option>
			</select>
			</div>
		</div>
		<div class="col-md-2">
			<span class="checkbox-filter"><a href="#" data-toggle="tooltip" title="Internet"><input type="checkbox" name="wifi" id="wifi2" value="da"> <label for="wifi2"><span class="icon-wi-fi"></span></label> </a></span>
			<span class="checkbox-filter"><a href="#" data-toggle="tooltip" title="Televizor"><input type="checkbox" name="tv" id="tv2" value="da"> <label for="tv2"><span class="icon-tv"></span></label> </a></span>
			<span class="checkbox-filter"><a href="#" data-toggle="tooltip" title="Klima"><input type="checkbox" name="ac" id="ac2" value="da"> <label for="ac2"><span class="icon-ac"></span></label> </a></span>
			<span class="checkbox-filter"><a href="#" data-toggle="tooltip" title="Parking"><input type="checkbox" name="parking" id="parking2" value="da"> <label for="parking2"><span class="icon-parking"></span></label> </a></span>
			<span class="checkbox-filter"><a href="#" data-toggle="tooltip" title="Bazen"><input type="checkbox" name="bazen" id="bazen2" value="da"> <label for="bazen2"><span class="icon-summer"></span></label> </a></span>
			<span class="checkbox-filter"><a href="#" data-toggle="tooltip" title="Lift"><input type="checkbox" name="lift" id="lift2" value="da"> <label for="lift2"><span class="icon-elevator"></span></label> </a></span>
			<span class="checkbox-filter"><a href="#" data-toggle="tooltip" title="Kućni ljubimci"><input type="checkbox" name="ljubimci" id="ljubimci2" value="da"> <label for="ljubimci2"><span class="icon-pets"></span></label> </a></span>
			<span class="checkbox-filter"><a href="#" data-toggle="tooltip" title="Pristup za invalide"><input type="checkbox" name="pristup_invalidima" id="pristup_invalidima2" value="da"> <label for="pristup_invalidima2"><span class="icon-wheelchair-access"></span></label> </a></span>
			<span class="checkbox-filter"><a href="#" data-toggle="tooltip" title="Wellness"><input type="checkbox" name="wellness" id="wellness2" value="da"> <label for="wellness2"><span class="icon-massage"></span></label> </a></span>
		</div>
		<div class="col-md-2 zalepi-dole">
			<div id="plaza-hoteli">
				<span class="filter-subheading"><span class="icon-distance"></span><br>Udaljenost od plaže:</span>
				<select name="odPlaze" id="udaljenostPlaza-hoteli">
					<option value="nebitno">Nebitno</option>
					<option value="naPlazi">Na plaži</option>
					<option value="do200">Do 200m</option>
					<option value="do400">Do 400m</option>
				</select>
			</div>
			<div id="gondola-hoteli" style="display:none">
				<span class="filter-subheading"><span class="icon-distance"></span><br>Udaljenost od gondole:</span>
				<select name="odGondole" id="udaljenostGondola-hoteli">
					<option value="nebitno">Nebitno</option>
					<option value="kodGondole">Kod gondole</option>
					<option value="do200">Do 200m</option>
					<option value="do400">Do 400m</option>
				</select>
			</div>
		</div>
		<div class="col-md-2">
		<span class="filter-subheading"><span class="icon-room-service"></span><br>Usluga hotela:</span>
			<select name="uslugaHotela" id="uslugaHotela">
				<option value="">Bilo koje</option>
				<option value="NA">NA</option>
				<option value="ND">ND</option>
				<option value="PP">PP</option>
				<option value="PA">PA</option>
				<option value="ALL">ALL</option>
			</select>
		</div>
		<div class="col-md-2">
		<span class="filter-subheading"> <br> <br></span>
			<button type="submit" name="submit" id="searchsubmit2" value=""><span class="icon-magnifier"></span> Pretraži</button>
		</div>	
	</div>
</form>