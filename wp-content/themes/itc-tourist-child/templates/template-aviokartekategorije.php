<?php



/**

 * Template Name: Avio karte - Sve kategorije

 *

 * @package TA Pluton

 */



global $post;

$currentID = $post->ID;

$pages_no = get_pages("child_of=$currentID&depth=1&parent=$currentID");

$count = count($pages_no);



get_header(); ?>



<!-- Ako ima header slika -->





	<div id="header-image">

		<?php if (get_field('header_image')) : ?>

			<img src="<?php the_field('header_image'); ?>" class="object-fit-center" />

			<?php $noHeaderClass = '' ?>

		<?php else : ?>

			<img src="<?php the_field('pozadina_drzavadestinacija','option'); ?>" class="object-fit-center" />

			<?php $noHeaderClass = 'no-header-class' ?>

		<?php endif; ?>

		<div class="white-bg"></div>



		<div class="container position-absolute d-flex justify-content-between">



			<div class="d-flex flex-column title">

				

				<?php 

				$tip_templatea = get_field('drzava_ili_oblast');

				

				

				?>

				<?php if ($count > 1 && $count < 5) {

					the_title('<h1 class="entry-title text-uppercase '. $noHeaderClass .'">', '</h1> <span class="br-destinacija"><i class="icon-home mr-2"></i> ' . $count . ' '.$tip_templatea.' u ponudi</span>');

				} else {

					the_title('<h1 class="entry-title text-uppercase '. $noHeaderClass .'">', '</h1> <span class="br-destinacija"><i class="icon-home mr-2"></i> ' . $count . ' '.$tip_templatea.' u ponudi</span>');

				} ?>



				<?php if (function_exists('yoast_breadcrumb')) {

					yoast_breadcrumb('<p id="breadcrumbs">', '</p>');

				}

				?>

			</div>



			<?php if(get_field('embed_video')): ?>

			<div class="video-container d-flex justify-content-center align-items-center <?php echo $noHeaderClass; ?>" data-toggle="modal" data-target="#openVideo">

				<div class="icon-container">

					<div class="pulse"></div>

					<div class="icon-border <?php echo $noHeaderClass; ?>">

						<div class="icon <?php echo $noHeaderClass; ?>">

							<i class="icon-play <?php echo $noHeaderClass; ?>"></i>

						</div>

					</div>

				</div>

				<span class="ml-3">Pogledaj video</span>



				<!-- Modal -->

				<div class="modal fade" id="openVideo" tabindex="-1" role="dialog" aria-labelledby="openVideoTitle" aria-hidden="true">

					<div class="modal-dialog modal-dialog-centered modal-lg" role="document">

						<div class="modal-content">

							<div class="modal-header">

								<h5 class="modal-title" id="openVideoTitle"><?php the_field('naslov_video_klipa') ?></h5>

								<button type="button" class="close" data-dismiss="modal" aria-label="Close">

									<span aria-hidden="true">&times;</span>

								</button>

							</div>

							<div class="modal-body">

								<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php the_field('embed_video') ?>?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

							</div>

							<div class="modal-footer">

								<button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>

							</div>

						</div>

					</div>

				</div>



			</div>

			<?php endif; ?>

		</div>



	</div>





<div class="main-content">

	<div class="container">

		<div class="row">

			<div class="col-lg-9 site-content" role="main">



				<div id="expandable">

						<div class="card-header" id="headingOne">

							<p><?php the_field('kraci_opis') ?></p>

						</div>

						<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#expandable">

							<div class="card-body">

								<?php while (have_posts()) : the_post(); ?>

									<?php the_content(); ?>

									<?php $parent = $post->ID; ?>

								<?php endwhile; // end of the loop. 

								?>

							</div>

						</div>

						<span class="expand-btn mt-2 mb-4" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Prikaži više</span>

				</div>







				<?php $dest = (array(

					'post_type' => 'page',

					'orderby' => 'menu_order',

					'order' => 'ASC',

					'post_parent' => $parent,

					'posts_per_page' => 12,

					'paged' => $paged,

				));

				$dest = new WP_Query($dest);

				?>



				<?php if ($dest->have_posts()) : ?>



					<article class="destinacije-wrapper mb-5">



						<h3 class="text-uppercase font-weight-bold mb-4"><?php the_title(); ?> - Destinacije u ponudi:</h3>



						<div class="row destinacije destinacije-listing">

							<?php while ($dest->have_posts()) : $dest->the_post(); ?>

								<div class="col-xl-4 col-lg-4 col-md-6 posebno hover1">

									<a href="<?php the_permalink(); ?>" class="single-destinacija-drzave">

										<figure>

											<?php the_post_thumbnail('ponude-img', array('class' => 'object-fit-center')); ?>

										</figure>



										<h5><?php the_title(); ?></h5>

									</a>

								</div>

							<?php endwhile;

							wp_reset_postdata(); ?>



							<?php if ($dest->max_num_pages > 1) : // check if the max number of pages is greater than 1  

							?>

								<nav class="prev-next-posts">

									<div class="prev-posts-link">

										<?php echo get_next_posts_link('Older Entries', $dest->max_num_pages); // display older posts link 

										?>

									</div>

									<div class="next-posts-link">

										<?php echo get_previous_posts_link('Newer Entries'); // display newer posts link 

										?>

									</div>

								</nav>

							<?php endif; ?>

						</div>



					</article>



				<?php endif; ?>





			</div><!-- #main -->

			<div class="col-lg-3">

			<?php

				$posts = get_posts(array(

					"post_type"=>"aviokarte",

					'tax_query' => array(

						array(

						  'taxonomy' => 'aviokarte_cat',

						  'field' => 'id',

						  'terms' => array(12)

						)

					),

					"post_status"=>"publish",

					"posts_per_page"=>-1 

				));

				?>



				<?php if ($posts && count($posts)>0) : global $post; ?>

					<aside class="side-card mb-4">

					<div class="title">

						<h5 class="card-primary">Avio karte</h5>

					</div>

					<div class="items">

						<?php foreach ($posts as $post) : setup_postdata($post); 

						$featured_post = get_field('let_sidebar');

						?>

							<a href="" class="d-flex single-item justify-content-between align-items-center">

								<div class="d-flex align-items-center justify-content-center">

									<h6 class="m-0"><?php the_field('od_destinacija',$featured_post); ?></h6>

									<div class="airport">

										<i class="icon-surface1 main-color-text"></i>

									</div>

									<h6 class="m-0"><?php the_field('do_destinacija',$featured_post); ?></h6>

								</div>

								<div class="main-color-text"><?php the_field('cena_aviokarte'); ?></div>

							</a>

							

						<?php endforeach; wp_reset_postdata(); ?>

						<div class="single-item">

							<a href="<?php echo get_site_url(); ?>/globus/avio-karte/" class="second-btn w-100 mb-2">Pogledajte celokupnu ponudu</a>

						</div>

					</div>

					</aside>

				<?php endif; ?>



				<?php

				$posts = get_posts(array(

					"post_type"=>"aviokarte",

					'tax_query' => array(

						array(

						  'taxonomy' => 'aviokarte_cat',

						  'field' => 'id',

						  'terms' => array(11)

						)

						),

					"post_status"=>"publish",

					"posts_per_page"=>-1 

				));

				?>

				<?php if ($posts && count($posts)>0) : global $post; ?>

				<aside class="side-card side-card-special-avio mb-4">

					<div class="title">

						<h5>Letovi iz Niša</h5>

					</div>

					<div class="items">

						<?php foreach ($posts as $post) : setup_postdata($post); 

						$featured_post = get_field('let_sidebar');

						?>

						

						<a href="<?php the_permalink($featured_post); ?>" class="single-item d-flex flex-column justify-content-between">

							<div class="top d-flex justify-content-between">

								<div class="d-flex align-items-center">



									<h6 class="d-inline-block mr-1 text-uppercase"><?php the_field('do_destinacija',$featured_post); ?></h6>

									<span class="badge small-text"><?php the_sub_field('tekst_bedza','option'); ?></span>

								</div>

								<h6 class="d-flex align-items-center font-weight-bold">



									<?php the_field('cena_aviokarte'); ?>

								</h6>

							</div>

							<div class="bottom d-flex justify-content-between">

								<span class="small-text d-block"><?php the_field('komentar_leta') ?></span>

							</div>

						</a>

						<?php endforeach; wp_reset_postdata(); ?>

						<div class="single-item">

							<a href="<?php echo get_site_url(); ?>/globus/avio-karte/" class="second-btn w-100 mb-2">Pogledajte celokupnu ponudu</a>

						</div>

					</div>



				</aside>

				<?php endif; ?>

			</div>

		</div>



	</div> <!-- container end -->

</div>



<?php get_footer(); ?>