<?php

/**
 * Template Name: Kontaktiraj nas
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<div class="container" id="contact-us">
			<div class="row">
				<div class="col-md-6">
					<h1><?php the_title(); ?></h1>

					<?php $form =  get_field('kod_forme') ?>
					<?php echo do_shortcode($form); ?>
				</div>
				<div class="col-md-6 info">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
		<div class="mapa-firme w-100 ">
			<?php the_field('mapa_firme') ?>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
