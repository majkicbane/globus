<?php
require_once('../../../wp-load.php');

if(isset($_POST["sezona_id"]) && !empty($_POST["sezona_id"])){
    // $naslov = get_the_title( $_POST["sezona_id"] ,OBJECT, 'page'  );
    // $idStrane = get_page_by_title( $_POST["sezona_id"] ,OBJECT, 'page' );
    // var_dump('Ovo je id'. $idStrane);
    //Get all state data
    $dropdown_args = array(
        'post_type' => 'page',
        'depth' => 1,
        'name' => 'country',
        'id' => 'country',
        'child_of' => $_POST["sezona_id"],
        'value_field' => 'ID',
        'option_none_value' => '',
        'show_option_no_change' => 'Izaberite državu',
        'selected' => 0,
    );
    $pages = wp_dropdown_pages( $dropdown_args );
    }

if(isset($_POST["country_id"]) && !empty($_POST["country_id"])){
    //Get all state data
    $dropdown_args = array(
        'post_type' => 'page',
        'depth' => 1,
        'name' => 'state',
        'id' => 'state',
        'child_of' => $_POST['country_id'],
        'value_field' => 'ID',
        'option_none_value' => '',
        'show_option_no_change' => 'Izaberite destinaciju',
        'selected' => 0,
    );
    $pages = wp_dropdown_pages( $dropdown_args );
    }
?>