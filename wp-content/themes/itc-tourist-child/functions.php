<?php



function add_theme_scripts() {



/* CSS files */

  wp_enqueue_style( 'reset', get_stylesheet_directory_uri() . '/css/reset.css', array(), '1.1', 'all');



  wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css', array(), '1.1', 'all');



  // wp_enqueue_style( 'animate', get_stylesheet_directory_uri() . '/css/animate.css', array(), '1.1', 'all');



  wp_enqueue_style( 'mmenu-light', get_stylesheet_directory_uri() . '/css/mmenu-light.css', array(), '', 'all' );



  wp_enqueue_style( 'demo', get_stylesheet_directory_uri() . '/css/demo.css', array(), '', 'all' );



  wp_enqueue_style( 'owl-carousel', get_stylesheet_directory_uri() . '/css/owl.carousel.css', array(), '1.1', 'all');



 //wp_enqueue_style( 'flexslider', get_stylesheet_directory_uri() . '/css/flexslider.css', array(), '1.1', 'all');



  wp_enqueue_style( 'Google-font', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800&display=swap', array(), '', 'all' );



  //wp_enqueue_style( 'examples-css', get_stylesheet_directory_uri() . '/css/examples.css', array(), '', 'all' );



  wp_enqueue_style( 'custom', get_stylesheet_directory_uri() . '/css/custom.css', array(), '1.1', 'all');



  wp_enqueue_style( 'style', get_stylesheet_uri() );





  /* JS files */ 



  // wp_enqueue_script( 'scrolloverflow', get_stylesheet_directory_uri() . '/js/scrolloverflow.js', array('jquery'), 1.1, true );



  wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js', array ( 'jquery' ), 1.1, true);



  wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', array ( 'jquery' ), 1.1, true);



  wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', array ( 'jquery' ), 1.1, true);



  wp_enqueue_script( 'mmenu-light', get_stylesheet_directory_uri() . '/js/mmenu-light.js', array('jquery'), '', true );



  wp_enqueue_script( 'owl-carousel', get_stylesheet_directory_uri() . '/js/owl.carousel.js', array ( 'jquery' ), 1.1, true);



  wp_enqueue_script( 'particles', get_stylesheet_directory_uri() . '/js/particles.js', array ( 'jquery' ), 1.1, true);



  wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/js/custom.js', array ( 'jquery' ), 1.1, true);



}



add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );





/* admin css*/



add_action( 'admin_enqueue_scripts', 'load_admin_style' );



function load_admin_style() {



  wp_enqueue_style( 'admin_css', get_stylesheet_directory_uri() . '/css/admin-style.css', array(), '1.1', 'all' );



}











add_theme_support( 'post-thumbnails' );

add_image_size( 'ponude-img', 390, 275, true );

add_image_size( 'header-img', 1600 );







/* Theme Options */



if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(

		'page_title' 	=> 'Theme General Settings',

		'menu_title'	=> 'Theme Settings',

		'menu_slug' 	=> 'theme-general-settings',

		'capability'	=> 'edit_posts',

		'redirect'		=> false

	));



  acf_add_options_sub_page(array(

		'page_title' 	=> 'Napomene',

		'menu_title'	=> 'Napomene',

		'parent_slug'	=> 'theme-general-settings',

  ));

  

  acf_add_options_sub_page(array(

		'page_title' 	=> 'Novogodišnje ponude - Kartica',

		'menu_title'	=> 'Novogodišnje ponude - Kartica',

		'parent_slug'	=> 'theme-general-settings',

	));



	acf_add_options_sub_page(array(

		'page_title' 	=> 'Theme Header Settings',

		'menu_title'	=> 'Header',

		'parent_slug'	=> 'theme-general-settings',

	));



	acf_add_options_sub_page(array(

		'page_title' 	=> 'Theme Contact Settings',

		'menu_title'	=> 'Contact', 

		'parent_slug'	=> 'theme-general-settings',

	));



	acf_add_options_sub_page(array(

		'page_title' 	=> 'Theme Footer Settings',

		'menu_title'	=> 'Footer',

		'parent_slug'	=> 'theme-general-settings',

	));



  acf_add_options_sub_page(array(

		'page_title' 	=> 'Egipat polasci',

		'menu_title'	=> 'Egipat',

		'parent_slug'	=> 'theme-general-settings',

  ));

  

  acf_add_options_sub_page(array(

		'page_title' 	=> 'Dubai polasci',

		'menu_title'	=> 'Dubai',

		'parent_slug'	=> 'theme-general-settings',

  ));

  

  acf_add_options_sub_page(array(

		'page_title' 	=> 'Avio prevoznici',

		'menu_title'	=> 'Avio prevoznici',

		'parent_slug'	=> 'theme-general-settings',

	));



}







 











include( STYLESHEETPATH . '/inc/post-types/CPT.php'); 





/**

 * Cases Custom Post Type

 */



include( STYLESHEETPATH . '/inc/post-types/first-minut.php');



// /**

//  * Applications Custom Post Type

//  */



include( STYLESHEETPATH . '/inc/post-types/last-minut.php');



// /**

//  * Faq Custom Post Type

//  */



include( STYLESHEETPATH . '/inc/post-types/aviokarte.php');



// /**

//  * Team Custom Post Type

//  */



// include( STYLESHEETPATH . '/inc/post-types/register-team.php');







function wpb_widgets_init() {

  register_sidebar( array(

    'name'          => 'Second navbar',

    'id'            => 'second-navbar',

    'before_widget' => '<div class="chw-widget">',

    'after_widget'  => '</div>',

    'before_title'  => '<h4 class="chw-title">',

    'after_title'   => '</h4>',

  ) );

  register_sidebar( array(

      'name'          => 'Footer 1',

      'id'            => 'footer-1',

      'before_widget' => '<div class="chw-widget">',

      'after_widget'  => '</div>',

      'before_title'  => '<h4 class="chw-title">',

      'after_title'   => '</h4>',

  ) );

  register_sidebar( array(

      'name'          => 'Footer 2',

      'id'            => 'footer-2',

      'before_widget' => '<div class="chw-widget">',

      'after_widget'  => '</div>',

      'before_title'  => '<h4 class="chw-title">',

      'after_title'   => '</h4>',

  ) );

  register_sidebar( array(

      'name'          => 'Footer 3',

      'id'            => 'footer-3',

      'before_widget' => '<div class="chw-widget">',

      'after_widget'  => '</div>',

      'before_title'  => '<h4 class="chw-title">',

      'after_title'   => '</h4>',

  ) );

  register_sidebar( array(

    'name'          => 'Footer 4',

    'id'            => 'footer-4',

    'before_widget' => '<div class="chw-widget">',

    'after_widget'  => '</div>',

    'before_title'  => '<h4 class="chw-title">',

    'after_title'   => '</h4>',

  ) );

  register_sidebar( array(

    'name'          => 'Footer 5',

    'id'            => 'footer-5',

    'before_widget' => '<div class="chw-widget">',

    'after_widget'  => '</div>',

    'before_title'  => '<h4 class="chw-title">',

    'after_title'   => '</h4>',

  ) );

}

add_action( 'widgets_init', 'wpb_widgets_init' );







add_action( 'wp' , function(){

  remove_filter( 'wp_nav_menu', 'twentynineteen_add_ellipses_to_nav', 10, 2 );

  remove_filter( 'walker_nav_menu_start_el', 'twentynineteen_add_dropdown_icons', 10, 4 );

  remove_filter( 'nav_menu_link_attributes', 'twentynineteen_nav_menu_link_attributes', 10, 4 );

  remove_filter( 'wp_nav_menu_objects', 'twentynineteen_add_mobile_parent_nav_menu_items', 10, 2 );

});



// Move Yoast to bottom

function yoasttobottom() {

	return 'low';

}

add_filter( 'wpseo_metabox_prio', 'yoasttobottom');





// Select za posttype iz globalnog polja

function acf_load_avio_preoznici( $field ) {

  // reset choices

  $field['choices'] = array();

// if has rows

  if( have_rows('prevoznici', 'option') ) {

      // while has rows

      while( have_rows('prevoznici', 'option') ) {

          // instantiate row

          the_row();

            // vars

            $value = get_sub_field('logo');

            $label = get_sub_field('naziv');

                  // append to choices

            $field['choices'][ $value ] = $label;

      }

}

  // return the field

  return $field;

}

add_filter('acf/load_field/name=prevoznik', 'acf_load_avio_preoznici');





// Select za posttype iz globalnog polja

function acf_load_napomene( $field ) {

  // reset choices

  $field['choices'] = array();

// if has rows

  if( have_rows('napomene_global', 'option') ) {

      // while has rows

      while( have_rows('napomene_global', 'option') ) {

          // instantiate row

          the_row();

            // vars

            $value = get_sub_field('naziv');

            $label = get_sub_field('naziv');

                  // append to choices

            $field['choices'][ $value ] = $label;

      }

}

  // return the field

  return $field;

}

add_filter('acf/load_field/name=napomena_objekat', 'acf_load_napomene');