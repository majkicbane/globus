��          �   %   �      0     1     >     G     \  /   a     �     �     �     �     �     �     �  	   �     �     �     �  H     $   M  R   r  ,   �     �     	       X  )     �  	   �     �     �  0   �  	   �     �            	        %  	   .  
   8  !   C  	   e     o  |   w  "   �  j     "   �     �     �     �            
                                   	                                                                                %1$s at %2$s Comments Comments are closed. Edit Edit <span class="screen-reader-text">%s</span> Footer Footer Menu Leave a comment Page Pages: Primary Search for: Secondary Social Links Menu To the top %s Up %s We found %s result for your search. We found %s results for your search. Your comment is awaiting moderation. comments title%1$s reply on &ldquo;%2$s&rdquo; %1$s replies on &ldquo;%2$s&rdquo; comments titleOne reply on &ldquo;%s&rdquo; https://wordpress.org/ search again the WordPress team PO-Revision-Date: 2019-10-05 14:37:09+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.4.0-alpha
Language: bs_BA
Project-Id-Version: Themes - Twenty Twenty
 %1$s u %2$s Komentari Komentari su onemogućeni. Uredi Uredi <span class="screen-reader-text">%s</span> Podnožje Meni u podnožju Komentariši Stranica Stranice: Primarno Pretraga: Sekundarno Meni linkova za društvene mreže Na vrh %s Gore %s Za vašu pretragu smo našli %s rezultat Za vašu pretragu smo našli %s rezultata Za vašu pretragu smo našli %s rezultata Vaš komentar čeka na moderaciju. %1$s odgovor na &ldquo;%2$s&rdquo; %1$s odgovora na &ldquo;%2$s&rdquo; %1$s odgovora na &ldquo;%2$s&rdquo; Jedan komentar na &ldquo;%s&rdquo; https://bs.wordpress.org/ traži ponovo WordPress tim 