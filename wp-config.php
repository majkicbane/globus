<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'globus' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lNAF?8q69toLh%AIg9lyIgS/6Fqm*@ C/i1>%_BJ<cn)I{`Cb=B1>8eo=$U7^GoM' );
define( 'SECURE_AUTH_KEY',  '_*$aIb?:qVJk(<&g7!&hEkN7E2/bj)Y(h5AR+b]5q?XL7hbIi- 0ud:fC=M&OvxU' );
define( 'LOGGED_IN_KEY',    '8qLUgcB/]N<cZ<4PcqPCH}1}M@&]G[F3(Z*-}Ed-+MK!I5->vr}Z17tD9rvTdqjP' );
define( 'NONCE_KEY',        'qf3!b`Qi/)_1&lK8_L3WV}-[.,t2RJfGMD)DvP9p2_4cr|8WDrU6@n<gK}ANCK~5' );
define( 'AUTH_SALT',        ':H,<cpXl4W-No6C50@f`:-.Zo&DSGjl)Qv2,%[,g5c2?;CI^toD=QT}[04`Q,LJf' );
define( 'SECURE_AUTH_SALT', 'k3bC^$cx0a%lc4vHqbE%P,w#HF| Th!d~DVG$5!:1_kJN3[9M,]^6^JlFY8+CqBO' );
define( 'LOGGED_IN_SALT',   'Ki9,3qf0c,LX5vEbT3*[(-bx9s/;Ee+^}3Iu+6ou4~_+?uJZ)*zwaZZ%V)n3#7Y`' );
define( 'NONCE_SALT',       'tO:X(P<|L*DX&qmMZ8WERm`j3/F.:5ND|WEr1{I9Vctun8UW8DxD/Oo(bY*y0iDW' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
